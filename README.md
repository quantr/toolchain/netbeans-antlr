# A new Netbeans antlr plugin

Doc in here https://www.quantr.foundation/docs/?project=Netbeans+Antlr&url=https%3A%2F%2Fgitlab.com%2Fquantr%2Ftoolchain%2Fnetbeans-antlr%2Fraw%2Fmain%2Fdoc%2FIntroduction.MD&docPath=Introduction.MD

![](https://www.quantr.foundation/wp-content/uploads/2022/06/netbeans-antlr6.png)

![](https://www.quantr.foundation/wp-content/uploads/2022/06/netbeans-antlr1.png)

![](https://www.quantr.foundation/wp-content/uploads/2022/06/netbeans-antlr2.png)

![](https://www.quantr.foundation/wp-content/uploads/2022/06/netbeans-antlr3.png)

![](https://www.quantr.foundation/wp-content/uploads/2022/06/netbeans-antlr4.png)

![](https://www.quantr.foundation/wp-content/uploads/2022/06/netbeans-antlr5.png)

# Build this project

1. git clone https://github.com/antlr/Antlr4Formatter.git
1. cd Antlr4Formatter
1. mvn clean install
1. then just build this project by "mvn package"

# Architectural diagram

![](https://peter.quantr.hk/wp-content/uploads/2019/07/architecture.png)
