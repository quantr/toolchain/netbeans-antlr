# How to use

1. Open 4 things

a. your lexer file

b. your parser file

c. your testing file

d. antlr window by clicking "window" > "antlr"

like this:

![](https://www.quantr.foundation/wp-content/uploads/2023/07/2023-07-14_13-53.png)

2. Set it up

In windows, install dot by https://graphviz.org/download/

![](https://www.quantr.foundation/wp-content/uploads/2023/07/2023-07-14_14-01.png)

3. Use it

you can keep editing your grammar and the testing file, it auto refresh and showing you the token table and parse tree

![](https://www.quantr.foundation/wp-content/uploads/2023/07/2023-07-14_14-04.png)

![](https://www.quantr.foundation/wp-content/uploads/2023/07/2023-07-14_14-04_1.png)





