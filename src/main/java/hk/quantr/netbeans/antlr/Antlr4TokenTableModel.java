package hk.quantr.netbeans.antlr;

import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.table.DefaultTableModel;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.Vocabulary;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class Antlr4TokenTableModel extends DefaultTableModel {

	String columnNames[] = new String[]{"No", "Line"};
	public ArrayList<String> lines = new ArrayList<>();
	public HashMap<Integer, ArrayList<Token>> tokens = new HashMap<Integer, ArrayList<Token>>();
	public Vocabulary vocabulary;

	@Override
	public String getColumnName(int column) {
		if (columnNames == null) {
			return null;
		}
		if (column == 0) {
			return columnNames[0];
		} else if (column == 1) {
			return columnNames[1];
		} else {
			return null;
		}
	}

	@Override
	public int getColumnCount() {
		int size = findLongestTokenCount();
		return size >= 0 ? size + 2 : 0;
	}

	@Override
	public int getRowCount() {
		if (lines == null) {
			return 0;
		}
		return lines.size();
	}

	@Override
	public Object getValueAt(int row, int column) {
		try {
			if (column == 0) {
				return row + 1 + " ";
			} else if (column == 1) {
				return lines.get(row);
			} else if (column > 1) {
				int size = tokens.get(row + 1).size();
				if (column - 2 > size) {
					return null;
				} else {
					Token token = tokens.get(row + 1).get(column - 2);
					String tokenName = vocabulary.getSymbolicName(token.getType());
					if (tokenName == null) {
						return token.getText();
					} else {
						return tokenName;
					}
				}
			} else {
				return null;
			}
		} catch (Exception ex) {
			return "";
		}
	}

	public Token getToken(int row, int column) {
		return tokens.get(row + 1).get(column - 2);
	}

	int findLongestTokenCount() {
		if (tokens == null) {
			return 0;
		}
		int x = Integer.MIN_VALUE;
		for (Integer line : tokens.keySet()) {
			int size = tokens.get(line).size();
			if (size > x) {
				x = size;
			}
		}
		return x;
	}

	@Override
	public boolean isCellEditable(int row, int column) {
		return false;
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return Object.class;
	}
}
