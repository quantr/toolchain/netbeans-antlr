package hk.quantr.netbeans.antlr;

import hk.quantr.netbeans.antlr.ErrorListenerForDot.ErrorInfo;
import hk.quantr.netbeans.antlr.syntax.antlr4.AstNode;
import hk.quantr.javalib.CommonLib;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import javax.imageio.ImageIO;
import org.antlr.v4.runtime.Token;
import org.apache.commons.io.FileUtils;
import org.openide.loaders.DataObject;
import org.openide.util.Exceptions;
import org.openide.util.NbPreferences;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class AntlrLib {

	static int id = 0;

	public static void removeOneLeafNodes(AstNode node) {
//		for (int x = 0; x < node.getChildren().size(); x++) {
//			AstNode nn = node.getChild(x);
//			if (nn.getChildren().size() == 1) {
//				System.out.println("bingo " + processLabel(nn.getName(), nn.getLabel()));
//				AstNode nextNextNode = nn.getChild(0);
//				while (nextNextNode.getChildren().size() == 1) {
//					nextNextNode = nextNextNode.getChild(0);
//					System.out.println("\t\t\tmiddle " + processLabel(nextNextNode.getName(), nextNextNode.getLabel()));
//				}
//				System.out.println("\t\t\t\tfinal " + processLabel(nextNextNode.getName(), nextNextNode.getLabel()));
//				node.getChildren().remove(nn);
//				node.addChild(nextNextNode);
//			}
//		}

		//System.out.println("current=" + processLabel(node));
		for (int x = 0; x < node.getChildren().size(); x++) {
			AstNode nextNode = node.getChild(x);

			//System.out.println("\tnextNode=" + processLabel(nextNode));
			String nextNodeLabel = nextNode.getEscapedLabel();//processLabel(nextNode);
//			if (nextNodeLabel.equals("STRING")) {
//				System.out.println("break");
//			}

			if (nextNode.getChildrenSize() == 1) {
				AstNode nextNextNode = nextNode.getChild(0);
				String nextNextNodeLabel = nextNextNode.getEscapedLabel();//processLabel(nextNextNode);
				if (!nextNodeLabel.equals(nextNextNodeLabel)) {
					break;
				}

				while (nextNextNode.getChildrenSize() == 1) {
					String temp = nextNextNode.getChild(0).getEscapedLabel();// processLabel(nextNextNode.getChild(0));
					if (!nextNextNodeLabel.equals(temp)) {
						break;
					}

					nextNextNode = nextNextNode.getChild(0);
				}
				//System.out.println("\t\tremove " + processLabel(node) + " -> " + processLabel(nextNextNode));

				node.getChildren().set(x, nextNextNode);
			}
		}

		for (int x = 0; x < node.getChildren().size(); x++) {
			AstNode nextNode = node.getChild(x);
			removeOneLeafNodes(nextNode);
		}
	}

	public static void filterUnwantedSubNodes(AstNode node, String[] unwantedString) {
		String realLabel = node.getLabel().replaceAll("\\\\t", "\\\\\\t").replaceAll("\\\\n", "\\\\\\n");
		if (realLabel.contains(":")) {
			realLabel = realLabel.split(":")[0];
		}
		for (String s : unwantedString) {
			if (node.getName().toLowerCase().contains("ruleblock")) {
				node.getChildren().clear();
				return;
			}
		}
		for (AstNode nn : node.getChildren()) {
			filterUnwantedSubNodes(nn, unwantedString);
		}
	}

	public static void printAst(String prefix, AstNode node) {
		String realLabel = node.getLabel().replaceAll("\\\\t", "\\\\\\t").replaceAll("\\\\n", "\\\\\\n");
		if (realLabel.contains(":")) {
			String s[] = realLabel.split(":");
			if (s.length > 0) {
				realLabel = s[0];
			}
		}
		System.out.println(prefix + node.getName() + "[" + realLabel + "]");
		for (AstNode nn : node.getChildren()) {
			printAst(prefix + "    ", nn);
		}
	}

	public static String exportDot(AstNode node, ArrayList<ErrorInfo> errorInfos, String searchStr) {
		return exportDot(node, errorInfos, 50, searchStr);
	}

	public static String exportDot(AstNode node, ArrayList<ErrorInfo> errorInfos, int maxLabelLength, String searchStr) {
		String backgroundColor = (errorInfos == null || errorInfos.size() == 0) ? "white" : "#ffecec";
		StringBuffer sb = new StringBuffer();
		sb.append("digraph \"Grammar\" {\n"
				+ "	graph [	"
				+ "		fontname = \"Arial\",\n"
				+ "		splines  = ortho,\n"
				+ "		fontsize = 8,"
				+ "		rankdir=\"LR\",\n"
				+ "		bgcolor=\"" + backgroundColor + "\",\n"
				+ "	];\n"
				+ "	node [	"
				+ "		shape    = \"record\",\n"
				+ "     style    = \"filled\",\n"
				+ "		fontname = \"Arial\"\n"
				+ "];\n");

		for (AstNode nn : node.getChildren()) {
			exportDotChild(node.getName(), nn, sb, errorInfos, maxLabelLength, searchStr);
		}
		sb.append("}\n");
		return sb.toString();
	}

	public static void addText(File file) {
		try {
			BufferedImage image = ImageIO.read(file);
			Graphics g = image.getGraphics();
			((Graphics2D) g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			g.setFont(new Font("Verdana", Font.PLAIN, 12));
			g.setColor(Color.lightGray);
			g.fillRect(0, 0, 180, 30);
			g.setColor(Color.black);
			g.drawString(new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date()), 10, 20);
			g.dispose();
			ImageIO.write(image, "png", file);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static void exportDotChild(String currentNodeText, AstNode node, StringBuffer sb, ArrayList<ErrorInfo> errorInfos, int maxLabelLength, String searchStr) {
		String nodeLabel = node.getEscapedLabel();//processLabel(node);
		if (maxLabelLength > 0 && nodeLabel.length() > maxLabelLength) {
			nodeLabel = nodeLabel.substring(0, maxLabelLength) + " ...";
		}
		String nodeID = node.getName() + "-" + (id++);
		Color color = CommonLib.getLightColor(node.getName(), 0.8f);
		String colorStr;
		String shape;
		if (isErrorNode(errorInfos, node.getSidx(), node.getEidx())) {
			shape = "record";
			colorStr = "#ffa8a8";
			sb.append("\"" + nodeID + "\" [label=<<table>"
					+ "<tr><td>" + node.getName() + "</td></tr>"
					+ "<tr><td>" + nodeLabel + "</td></tr>"
					+ "<tr><td bgcolor=\"#c56161\">" + getErrorMessage(errorInfos, node.getSidx(), node.getEidx()) + "</td></tr>"
					+ "</table>>, color=\"" + colorStr + "\", shape=\"" + shape + "\"]\n");
		} else {
			shape = "record";
			colorStr = CommonLib.toHexColor(color);
			String nodeDisplay = node.getName() + "|" + nodeLabel;
			if (!searchStr.trim().equals("") && Arrays.stream(searchStr.toLowerCase().split(",")).anyMatch(node.getName().toLowerCase()::contains)) {
				sb.append("\"" + nodeID + "\" [label=\"" + nodeDisplay + "\", color=\"red\" fillcolor=\"" + colorStr + "\", shape=\"" + shape + "\", style=\"filled, setlinewidth(3)\"]\n");
			} else {
				sb.append("\"" + nodeID + "\" [label=\"" + nodeDisplay + "\", color=\"" + colorStr + "\", shape=\"" + shape + "\"]\n");
			}
		}

		sb.append("\"" + currentNodeText + "\" -> \"" + nodeID + "\"\n");
		for (AstNode nn : node.getChildren()) {
			exportDotChild(nodeID, nn, sb, errorInfos, maxLabelLength, searchStr);
		}
	}

//	public static String processLabel(AstNode node) {
//		String rule = node.getName();
//		String label = node.getEscapedLabel();
//		if (label.contains(":")) {
//			String s[] = label.split(":");
//			if (s.length > 0) {
//				label = label.split(":")[0];
//			}
//		}
//		//String realLabel = rule.equals("rules") ? rule : rule + "[" + label + "]";//.replaceAll("\\\\t", "\\\\\\t").replaceAll("\\\\n", "\\\\\\n");
//		String realLabel = rule.equals("rules") ? rule : label;
//
//		realLabel = realLabel.replace("\"", "\\\"");
//		if (realLabel.equals("")) {
//			realLabel = "none";
//		}
//		return realLabel;
//	}
	public static void buildTree(AstNode node, AntlrTreeNode rootNode) {
		for (AstNode nn : node.getChildren()) {
			String realLabel = nn.getEscapedLabel();//processLabel(nn);

			AntlrTreeNode newNode = new AntlrTreeNode(realLabel, realLabel);
			rootNode.add(newNode);
			newNode.setParent(rootNode);
			buildTree(nn, newNode);
		}
	}

	public static File getTargetFile(DataObject dataObject) {
		File currentFile = new File(dataObject.getPrimaryFile().getPath());
		String targetFilePath = NbPreferences.forModule(FileTypeG4VisualElement.class).get("file-" + currentFile.getAbsolutePath(), null);

		String preProcessVerilogCheckBox = NbPreferences.forModule(FileTypeG4VisualElement.class).get("preProcessVerilogCheckBox", null);
		boolean preProcessVerilogCheckBoxB = false;
		if (preProcessVerilogCheckBox != null && preProcessVerilogCheckBox.equals("true")) {
			preProcessVerilogCheckBoxB = true;
		}

		if (preProcessVerilogCheckBoxB) {
			try {
				InputStream is = FileTypeG4VisualElement.class.getClassLoader().getResourceAsStream("/hk/quantr/netbeans/antlr/preprocess/verilog-compiler-2.0.jar");
				Path jarPath = Files.createTempFile("verilog-compiler", ".jar");
				FileUtils.copyInputStreamToFile(is, jarPath.toFile());
				String command;
				String osName = System.getProperty("os.name").toLowerCase();
				if (osName.toLowerCase().contains("windows")) {
					command = "\"" + System.getProperty("java.home") + "/bin/java\" -jar " + jarPath + " -m " + targetFilePath;
				} else {
					command = System.getProperty("java.home") + "/bin/java -jar " + jarPath + " -m " + targetFilePath;
				}
				Path newFile = Files.createTempFile(null, null);
				FileUtils.writeStringToFile(newFile.toFile(), CommonLib.runCommand(command), "utf-8");
				return newFile.toFile();
			} catch (Exception ex) {
				Exceptions.printStackTrace(ex);
				return null;
			}
		} else {
			if (targetFilePath == null) {
				return null;
			} else {
				return new File(targetFilePath);
			}
		}
	}

	public static File getLexerGrammarFile(DataObject dataObject) {
		File currentFile = new File(dataObject.getPrimaryFile().getPath());
		String temp = NbPreferences.forModule(FileTypeG4VisualElement.class).get("fileLexer-" + currentFile.getAbsolutePath(), null);
		if (temp == null) {
			return null;
		} else {
			return new File(temp);
		}
	}

	public static String getStartRule(DataObject dataObject) {
		String startRule = FileTypeG4VisualElement.startRules.get(dataObject);
		if (startRule == null) {
			File currentFile = new File(dataObject.getPrimaryFile().getPath());
			if (currentFile == null) {
				return null;
			}
			startRule = NbPreferences.forModule(FileTypeG4VisualElement.class).get("startRule-" + currentFile.getAbsolutePath(), null);
		}
		return startRule;
	}

	private static boolean isErrorNode(ArrayList<ErrorInfo> errorInfos, int sidx, int eidx) {
		for (ErrorInfo errorInfo : errorInfos) {
			Token offendingToken = (Token) errorInfo.offendingSymbol;
			if (offendingToken == null) {
				return false;
			}
			int start = offendingToken.getStartIndex();
			int stop = offendingToken.getStopIndex();
			System.out.println(start + " == " + sidx + " && " + stop + " == " + eidx);
			if (start == sidx) {
				return true;
			}
		}
		return false;
	}

	private static String getErrorMessage(ArrayList<ErrorInfo> errorInfos, int sidx, int eidx) {
		for (ErrorInfo errorInfo : errorInfos) {
			Token offendingToken = (Token) errorInfo.offendingSymbol;
			if (offendingToken == null) {
				return null;
			}
			int start = offendingToken.getStartIndex();
			int stop = offendingToken.getStopIndex();
			System.out.println(start + " == " + sidx + " && " + stop + " == " + eidx);
			if (start == sidx) {
				return errorInfo.msg;
			}
		}
		return null;
	}

}
