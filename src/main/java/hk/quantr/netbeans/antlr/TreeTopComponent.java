package hk.quantr.netbeans.antlr;

import hk.quantr.netbeans.antlr.syntax.antlr4.Ast;
import hk.quantr.netbeans.antlr.syntax.antlr4.AstNode;
import hk.quantr.netbeans.antlr.syntax.antlr4.jgraph.MyGraphComponent;
import hk.quantr.netbeans.antlr.syntax.antlr4.Antlr4GeneralParserListener;
import hk.quantr.netbeans.antlr.syntax.antlr4.MyANTLRv4ParserListener;
import hk.quantr.netbeans.antlr.syntax.antlr4.jgraph.MyJGraphCanvas;
import hk.quantr.netbeans.antlr.syntax.antlr4.realtimecompile.CaretListenerForTargetTable;
import hk.quantr.mxgraph.canvas.mxICanvas;
import hk.quantr.mxgraph.canvas.mxImageCanvas;
import hk.quantr.mxgraph.layout.hierarchical.mxHierarchicalLayout;
import hk.quantr.mxgraph.layout.mxCircleLayout;
import hk.quantr.mxgraph.layout.mxCompactTreeLayout;
import hk.quantr.mxgraph.layout.mxEdgeLabelLayout;
import hk.quantr.mxgraph.layout.mxFastOrganicLayout;
import hk.quantr.mxgraph.layout.mxOrganicLayout;
import hk.quantr.mxgraph.layout.mxParallelEdgeLayout;
import hk.quantr.mxgraph.layout.mxStackLayout;
import hk.quantr.mxgraph.layout.orthogonal.mxOrthogonalLayout;
import hk.quantr.mxgraph.model.mxCell;
import hk.quantr.mxgraph.swing.util.mxMorphing;
import hk.quantr.mxgraph.util.mxConstants;
import hk.quantr.mxgraph.util.mxEvent;
import hk.quantr.mxgraph.util.mxEventObject;
import hk.quantr.mxgraph.util.mxEventSource;
import hk.quantr.mxgraph.view.mxCellState;
import hk.quantr.mxgraph.view.mxGraph;
import hk.quantr.javalib.CommonLib;
import hk.quantr.javalib.swing.advancedswing.jprogressbardialog.JProgressBarDialog;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JMenuItem;
import javax.swing.JViewport;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.antlr.parser.ANTLRv4Lexer;
import org.antlr.parser.ANTLRv4Parser;
import org.antlr.v4.Tool;
import org.antlr.v4.parse.ANTLRParser;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.LexerInterpreter;
import org.antlr.v4.runtime.ParserInterpreter;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.antlr.v4.tool.Grammar;
import org.antlr.v4.tool.Rule;
import org.antlr.v4.tool.ast.GrammarRootAST;
import org.apache.commons.io.FileUtils;
import org.netbeans.api.editor.EditorRegistry;
import org.netbeans.modules.editor.NbEditorUtilities;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.cookies.EditorCookie;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.windows.TopComponent;
import org.openide.util.NbBundle.Messages;
import org.openide.util.Exceptions;
import org.openide.util.NbPreferences;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.tool.LexerGrammar;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * Top component which displays something.
 */
//@ConvertAsProperties(
//		dtd = "-//hk.quantr.netbeans.antlr//Tree//EN",
//		autostore = false
//)
@TopComponent.Description(
		preferredID = "TreeTopComponent",
		iconBase = "hk/quantr/netbeans/antlr/antlr.png",
		persistenceType = TopComponent.PERSISTENCE_ALWAYS
)
@TopComponent.Registration(mode = "output", openAtStartup = false)
@ActionID(category = "Window", id = "hk.quantr.netbeans.antlr.TreeTopComponent")
@ActionReference(path = "Menu/Window" /*, position = 333 */)
@TopComponent.OpenActionRegistration(
		displayName = "#CTL_TreeAction",
		preferredID = "TreeTopComponent"
)
@Messages({
	"CTL_TreeAction=Antlr",
	"CTL_TreeTopComponent=Antlr Window",
	"HINT_TreeTopComponent=This is a Antlr window"
})
public final class TreeTopComponent extends TopComponent /*implements LookupListener*/ {

//	Lookup.Result<DataObject> result;
//	public static DataObject lastDataObject;
	File pngFileName;
	File pngTargetGraphFileName;
	int preferWidth = -1;
	int preferHeight;
	AntlrTreeNode rootNode = new AntlrTreeNode("Grammar", "Grammar");
	AntlrTreeModel treeModel = new AntlrTreeModel(rootNode);
	String dot;
	int xDiff, yDiff;
	boolean isDragging;
	Container c;

	mxGraph graph = new mxGraph() {
		@Override
		public boolean isCellResizable(Object cell) {
			return false;
		}

		@Override
		public boolean isCellMovable(Object cell) {
			return false;
		}

		@Override
		public boolean isCellEditable(Object cell) {
			return false;
		}

		@Override
		public String getToolTipForCell(Object cell) {
			if (model.isEdge(cell)) {
				return convertValueToString(model.getTerminal(cell, true)) + " -> " + convertValueToString(model.getTerminal(cell, false));
			}

			return super.getToolTipForCell(cell);
		}

		@Override
		public boolean isCellFoldable(Object cell, boolean collapse) {
			return false;
		}

		@Override
		public void drawState(mxICanvas canvas, mxCellState state, boolean drawLabel) {
			mxCell mxCell = (mxCell) state.getCell();
			if (mxCell != null) {
//				if (mxCell.getValue() != null) {
//					ModuleLib.log(mxCell.getValue().getClass());
//				}
				if (mxCell.getValue() instanceof AstNode) {
					AstNode node = (AstNode) ((mxCell) state.getCell()).getValue();

					if (getModel().isVertex(state.getCell()) && canvas instanceof mxImageCanvas && ((mxImageCanvas) canvas).getGraphicsCanvas() instanceof MyJGraphCanvas) {
						((MyJGraphCanvas) ((mxImageCanvas) canvas).getGraphicsCanvas()).drawVertex(state, node);
					} else if (getModel().isVertex(state.getCell()) && canvas instanceof MyJGraphCanvas) {
						((MyJGraphCanvas) canvas).drawVertex(state, node);
					} else {
						super.drawState(canvas, state, drawLabel);
					}
				} else {
					super.drawState(canvas, state, drawLabel);
				}
			}
		}
	};
	MyGraphComponent graphComponent = new MyGraphComponent(graph);
	public Antlr4TokenTableModel tokenTableModel = new Antlr4TokenTableModel();

	public static TreeTopComponent treeTopComponent;

	public TreeTopComponent() {
		TreeTopComponent.treeTopComponent = this;
		initComponents();
		setName(Bundle.CTL_TreeTopComponent());
		setToolTipText(Bundle.HINT_TreeTopComponent());

		graphComponent.setConnectable(false);
		graphComponent.setBackground(Color.white);
		targetJGraphPanel.add(graphComponent, BorderLayout.CENTER);

//		result = Utilities.actionsGlobalContext().lookupResult(DataObject.class);
//		result.addLookupListener(this);
		antlrTree.setModel(treeModel);
		antlrTree.setCellRenderer(new AntlrTreeRenderer());
		antlrTree.setShowsRootHandles(true);

		layoutButton.removeAll();
		layoutButton.add(new JMenuItem("Compact Tree Layout"));
		layoutButton.add(new JMenuItem("Hierarchical Layout"));
		layoutButton.add(new JMenuItem("Circle Layout"));
		layoutButton.add(new JMenuItem("Organic Layout"));
		layoutButton.add(new JMenuItem("Edge Label Layout"));
		layoutButton.add(new JMenuItem("Fast Organic Layout"));
		layoutButton.add(new JMenuItem("Orthogonal Layout"));
		layoutButton.add(new JMenuItem("Parallel Edge Layout"));
		layoutButton.add(new JMenuItem("Stack Layout"));

		tokenTable.setDefaultRenderer(Object.class, new TokenTableCellRenderer());
	}

	/**
	 * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always regenerated by the Form
	 * Editor.
	 */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mainTabbedPane = new javax.swing.JTabbedPane();
        compilePanel = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        compileEditorPane = new javax.swing.JEditorPane();
        tokenPanel = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tokenTable = new javax.swing.JTable();
        targetJGraphPanel = new javax.swing.JPanel();
        jToolBar4 = new javax.swing.JToolBar();
        refreshTargetJgraphButton = new javax.swing.JButton();
        clearJGraphButton = new javax.swing.JButton();
        zoomOutJGraphButton = new javax.swing.JButton();
        show100ImageJGraphButton = new javax.swing.JButton();
        zoomInJGraphButton = new javax.swing.JButton();
        layoutButton = new hk.quantr.javalib.swing.advancedswing.jdropdownbutton.JDropDownButton();
        targetGraphPanel = new javax.swing.JPanel();
        jToolBar3 = new javax.swing.JToolBar();
        refreshTargetGraphvizButton = new javax.swing.JButton();
        zoomOutTargetGraphButton = new javax.swing.JButton();
        show75TargetGraphImageButton = new javax.swing.JButton();
        show100TargetGraphImageButton = new javax.swing.JButton();
        zoomInTargetGraphButton = new javax.swing.JButton();
        copyDotButton = new javax.swing.JButton();
        clearDotButton = new javax.swing.JButton();
        saveDotButton = new javax.swing.JButton();
        searchTextField = new javax.swing.JTextField();
        graphvizScrollPane1 = new javax.swing.JScrollPane();
        graphvizTargetGraphLabel = new javax.swing.JLabel();
        graphvizPanel = new javax.swing.JPanel();
        graphvizScrollPane = new javax.swing.JScrollPane();
        graphvizLabel = new javax.swing.JLabel();
        jToolBar1 = new javax.swing.JToolBar();
        refreshGraphvizButton = new javax.swing.JButton();
        zoomOutButton = new javax.swing.JButton();
        show100ImageButton = new javax.swing.JButton();
        zoomInButton = new javax.swing.JButton();
        graphvizFitWidthButton = new javax.swing.JButton();
        graphvizFitHeightButton = new javax.swing.JButton();
        treePanel = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        antlrTree = new javax.swing.JTree();
        jToolBar2 = new javax.swing.JToolBar();
        refreshAntlrTreeButton = new javax.swing.JButton();
        expandTreeButton = new javax.swing.JButton();
        collapseTreeButton = new javax.swing.JButton();
        searchAntlrTreeTextField = new hk.quantr.javalib.swing.advancedswing.searchtextfield.JSearchTextField();

        setLayout(new java.awt.BorderLayout());

        compilePanel.setLayout(new java.awt.BorderLayout());

        jScrollPane3.setViewportView(compileEditorPane);

        compilePanel.add(jScrollPane3, java.awt.BorderLayout.CENTER);

        mainTabbedPane.addTab(org.openide.util.NbBundle.getMessage(TreeTopComponent.class, "TreeTopComponent.compilePanel.TabConstraints.tabTitle"), compilePanel); // NOI18N

        tokenPanel.setLayout(new java.awt.BorderLayout());

        tokenTable.setModel(tokenTableModel);
        tokenTable.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        tokenTable.setCellSelectionEnabled(true);
        tokenTable.setGridColor(new java.awt.Color(220, 220, 220));
        tokenTable.setIntercellSpacing(new java.awt.Dimension(1, 0));
        tokenTable.setRowHeight(22);
        tokenTable.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tokenTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tokenTableMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tokenTable);

        tokenPanel.add(jScrollPane2, java.awt.BorderLayout.CENTER);

        mainTabbedPane.addTab(org.openide.util.NbBundle.getMessage(TreeTopComponent.class, "TreeTopComponent.tokenPanel.TabConstraints.tabTitle"), tokenPanel); // NOI18N

        targetJGraphPanel.setLayout(new java.awt.BorderLayout());

        jToolBar4.setRollover(true);

        org.openide.awt.Mnemonics.setLocalizedText(refreshTargetJgraphButton, org.openide.util.NbBundle.getMessage(TreeTopComponent.class, "TreeTopComponent.refreshTargetJgraphButton.text")); // NOI18N
        refreshTargetJgraphButton.setFocusable(false);
        refreshTargetJgraphButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        refreshTargetJgraphButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        refreshTargetJgraphButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                refreshTargetJgraphButtonActionPerformed(evt);
            }
        });
        jToolBar4.add(refreshTargetJgraphButton);

        org.openide.awt.Mnemonics.setLocalizedText(clearJGraphButton, org.openide.util.NbBundle.getMessage(TreeTopComponent.class, "TreeTopComponent.clearJGraphButton.text")); // NOI18N
        clearJGraphButton.setFocusable(false);
        clearJGraphButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        clearJGraphButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        clearJGraphButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clearJGraphButtonActionPerformed(evt);
            }
        });
        jToolBar4.add(clearJGraphButton);

        org.openide.awt.Mnemonics.setLocalizedText(zoomOutJGraphButton, org.openide.util.NbBundle.getMessage(TreeTopComponent.class, "TreeTopComponent.zoomOutJGraphButton.text")); // NOI18N
        zoomOutJGraphButton.setFocusable(false);
        zoomOutJGraphButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        zoomOutJGraphButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        zoomOutJGraphButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                zoomOutJGraphButtonActionPerformed(evt);
            }
        });
        jToolBar4.add(zoomOutJGraphButton);

        org.openide.awt.Mnemonics.setLocalizedText(show100ImageJGraphButton, org.openide.util.NbBundle.getMessage(TreeTopComponent.class, "TreeTopComponent.show100ImageJGraphButton.text")); // NOI18N
        show100ImageJGraphButton.setFocusable(false);
        show100ImageJGraphButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        show100ImageJGraphButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        show100ImageJGraphButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                show100ImageJGraphButtonActionPerformed(evt);
            }
        });
        jToolBar4.add(show100ImageJGraphButton);

        org.openide.awt.Mnemonics.setLocalizedText(zoomInJGraphButton, org.openide.util.NbBundle.getMessage(TreeTopComponent.class, "TreeTopComponent.zoomInJGraphButton.text")); // NOI18N
        zoomInJGraphButton.setFocusable(false);
        zoomInJGraphButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        zoomInJGraphButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        zoomInJGraphButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                zoomInJGraphButtonActionPerformed(evt);
            }
        });
        jToolBar4.add(zoomInJGraphButton);

        org.openide.awt.Mnemonics.setLocalizedText(layoutButton, org.openide.util.NbBundle.getMessage(TreeTopComponent.class, "TreeTopComponent.layoutButton.text")); // NOI18N
        layoutButton.setFocusable(false);
        layoutButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        layoutButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        layoutButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                layoutButtonActionPerformed(evt);
            }
        });
        jToolBar4.add(layoutButton);

        targetJGraphPanel.add(jToolBar4, java.awt.BorderLayout.PAGE_START);

        mainTabbedPane.addTab(org.openide.util.NbBundle.getMessage(TreeTopComponent.class, "TreeTopComponent.targetJGraphPanel.TabConstraints.tabTitle"), targetJGraphPanel); // NOI18N

        targetGraphPanel.setLayout(new java.awt.BorderLayout());

        jToolBar3.setRollover(true);

        org.openide.awt.Mnemonics.setLocalizedText(refreshTargetGraphvizButton, org.openide.util.NbBundle.getMessage(TreeTopComponent.class, "TreeTopComponent.refreshTargetGraphvizButton.text")); // NOI18N
        refreshTargetGraphvizButton.setFocusable(false);
        refreshTargetGraphvizButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        refreshTargetGraphvizButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        refreshTargetGraphvizButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                refreshTargetGraphvizButtonActionPerformed(evt);
            }
        });
        jToolBar3.add(refreshTargetGraphvizButton);

        org.openide.awt.Mnemonics.setLocalizedText(zoomOutTargetGraphButton, org.openide.util.NbBundle.getMessage(TreeTopComponent.class, "TreeTopComponent.zoomOutTargetGraphButton.text")); // NOI18N
        zoomOutTargetGraphButton.setFocusable(false);
        zoomOutTargetGraphButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        zoomOutTargetGraphButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        zoomOutTargetGraphButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                zoomOutTargetGraphButtonActionPerformed(evt);
            }
        });
        jToolBar3.add(zoomOutTargetGraphButton);

        org.openide.awt.Mnemonics.setLocalizedText(show75TargetGraphImageButton, org.openide.util.NbBundle.getMessage(TreeTopComponent.class, "TreeTopComponent.show75TargetGraphImageButton.text")); // NOI18N
        show75TargetGraphImageButton.setFocusable(false);
        show75TargetGraphImageButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        show75TargetGraphImageButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        show75TargetGraphImageButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                show75TargetGraphImageButtonActionPerformed(evt);
            }
        });
        jToolBar3.add(show75TargetGraphImageButton);

        org.openide.awt.Mnemonics.setLocalizedText(show100TargetGraphImageButton, org.openide.util.NbBundle.getMessage(TreeTopComponent.class, "TreeTopComponent.show100TargetGraphImageButton.text")); // NOI18N
        show100TargetGraphImageButton.setFocusable(false);
        show100TargetGraphImageButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        show100TargetGraphImageButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        show100TargetGraphImageButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                show100TargetGraphImageButtonActionPerformed(evt);
            }
        });
        jToolBar3.add(show100TargetGraphImageButton);

        org.openide.awt.Mnemonics.setLocalizedText(zoomInTargetGraphButton, org.openide.util.NbBundle.getMessage(TreeTopComponent.class, "TreeTopComponent.zoomInTargetGraphButton.text")); // NOI18N
        zoomInTargetGraphButton.setFocusable(false);
        zoomInTargetGraphButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        zoomInTargetGraphButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        zoomInTargetGraphButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                zoomInTargetGraphButtonActionPerformed(evt);
            }
        });
        jToolBar3.add(zoomInTargetGraphButton);

        org.openide.awt.Mnemonics.setLocalizedText(copyDotButton, org.openide.util.NbBundle.getMessage(TreeTopComponent.class, "TreeTopComponent.copyDotButton.text")); // NOI18N
        copyDotButton.setFocusable(false);
        copyDotButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        copyDotButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        copyDotButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                copyDotButtonActionPerformed(evt);
            }
        });
        jToolBar3.add(copyDotButton);

        org.openide.awt.Mnemonics.setLocalizedText(clearDotButton, org.openide.util.NbBundle.getMessage(TreeTopComponent.class, "TreeTopComponent.clearDotButton.text")); // NOI18N
        clearDotButton.setFocusable(false);
        clearDotButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        clearDotButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        clearDotButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clearDotButtonActionPerformed(evt);
            }
        });
        jToolBar3.add(clearDotButton);

        org.openide.awt.Mnemonics.setLocalizedText(saveDotButton, org.openide.util.NbBundle.getMessage(TreeTopComponent.class, "TreeTopComponent.saveDotButton.text")); // NOI18N
        saveDotButton.setFocusable(false);
        saveDotButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        saveDotButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        saveDotButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveDotButtonActionPerformed(evt);
            }
        });
        jToolBar3.add(saveDotButton);

        searchTextField.setText(org.openide.util.NbBundle.getMessage(TreeTopComponent.class, "TreeTopComponent.searchTextField.text")); // NOI18N
        searchTextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                searchTextFieldKeyPressed(evt);
            }
        });
        jToolBar3.add(searchTextField);

        targetGraphPanel.add(jToolBar3, java.awt.BorderLayout.PAGE_START);

        org.openide.awt.Mnemonics.setLocalizedText(graphvizTargetGraphLabel, org.openide.util.NbBundle.getMessage(TreeTopComponent.class, "TreeTopComponent.graphvizTargetGraphLabel.text_1")); // NOI18N
        graphvizTargetGraphLabel.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                graphvizTargetGraphLabelMouseDragged(evt);
            }
        });
        graphvizTargetGraphLabel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                graphvizTargetGraphLabelMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                graphvizTargetGraphLabelMouseReleased(evt);
            }
        });
        graphvizScrollPane1.setViewportView(graphvizTargetGraphLabel);

        targetGraphPanel.add(graphvizScrollPane1, java.awt.BorderLayout.CENTER);

        mainTabbedPane.addTab(org.openide.util.NbBundle.getMessage(TreeTopComponent.class, "TreeTopComponent.targetGraphPanel.TabConstraints.tabTitle"), targetGraphPanel); // NOI18N

        graphvizPanel.setLayout(new java.awt.BorderLayout());

        graphvizScrollPane.setViewportView(graphvizLabel);

        graphvizPanel.add(graphvizScrollPane, java.awt.BorderLayout.CENTER);

        jToolBar1.setRollover(true);

        org.openide.awt.Mnemonics.setLocalizedText(refreshGraphvizButton, org.openide.util.NbBundle.getMessage(TreeTopComponent.class, "TreeTopComponent.refreshGraphvizButton.text")); // NOI18N
        refreshGraphvizButton.setFocusable(false);
        refreshGraphvizButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        refreshGraphvizButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        refreshGraphvizButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                refreshGraphvizButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(refreshGraphvizButton);

        org.openide.awt.Mnemonics.setLocalizedText(zoomOutButton, org.openide.util.NbBundle.getMessage(TreeTopComponent.class, "TreeTopComponent.zoomOutButton.text")); // NOI18N
        zoomOutButton.setFocusable(false);
        zoomOutButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        zoomOutButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        zoomOutButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                zoomOutButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(zoomOutButton);

        org.openide.awt.Mnemonics.setLocalizedText(show100ImageButton, org.openide.util.NbBundle.getMessage(TreeTopComponent.class, "TreeTopComponent.show100ImageButton.text")); // NOI18N
        show100ImageButton.setFocusable(false);
        show100ImageButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        show100ImageButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        show100ImageButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                show100ImageButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(show100ImageButton);

        org.openide.awt.Mnemonics.setLocalizedText(zoomInButton, org.openide.util.NbBundle.getMessage(TreeTopComponent.class, "TreeTopComponent.zoomInButton.text")); // NOI18N
        zoomInButton.setFocusable(false);
        zoomInButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        zoomInButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        zoomInButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                zoomInButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(zoomInButton);

        org.openide.awt.Mnemonics.setLocalizedText(graphvizFitWidthButton, org.openide.util.NbBundle.getMessage(TreeTopComponent.class, "TreeTopComponent.graphvizFitWidthButton.text")); // NOI18N
        graphvizFitWidthButton.setFocusable(false);
        graphvizFitWidthButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        graphvizFitWidthButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        graphvizFitWidthButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                graphvizFitWidthButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(graphvizFitWidthButton);

        org.openide.awt.Mnemonics.setLocalizedText(graphvizFitHeightButton, org.openide.util.NbBundle.getMessage(TreeTopComponent.class, "TreeTopComponent.graphvizFitHeightButton.text")); // NOI18N
        graphvizFitHeightButton.setFocusable(false);
        graphvizFitHeightButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        graphvizFitHeightButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        graphvizFitHeightButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                graphvizFitHeightButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(graphvizFitHeightButton);

        graphvizPanel.add(jToolBar1, java.awt.BorderLayout.PAGE_START);

        mainTabbedPane.addTab(org.openide.util.NbBundle.getMessage(TreeTopComponent.class, "TreeTopComponent.graphvizPanel.TabConstraints.tabTitle"), graphvizPanel); // NOI18N

        treePanel.setLayout(new java.awt.BorderLayout());

        jScrollPane1.setViewportView(antlrTree);

        treePanel.add(jScrollPane1, java.awt.BorderLayout.CENTER);

        jToolBar2.setRollover(true);

        org.openide.awt.Mnemonics.setLocalizedText(refreshAntlrTreeButton, org.openide.util.NbBundle.getMessage(TreeTopComponent.class, "TreeTopComponent.refreshAntlrTreeButton.text_1")); // NOI18N
        refreshAntlrTreeButton.setFocusable(false);
        refreshAntlrTreeButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        refreshAntlrTreeButton.setLabel(org.openide.util.NbBundle.getMessage(TreeTopComponent.class, "TreeTopComponent.refreshAntlrTreeButton.text")); // NOI18N
        refreshAntlrTreeButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        refreshAntlrTreeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                refreshAntlrTreeButtonActionPerformed(evt);
            }
        });
        jToolBar2.add(refreshAntlrTreeButton);

        org.openide.awt.Mnemonics.setLocalizedText(expandTreeButton, org.openide.util.NbBundle.getMessage(TreeTopComponent.class, "TreeTopComponent.expandTreeButton.text_1")); // NOI18N
        expandTreeButton.setFocusable(false);
        expandTreeButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        expandTreeButton.setLabel(org.openide.util.NbBundle.getMessage(TreeTopComponent.class, "TreeTopComponent.expandTreeButton.text")); // NOI18N
        expandTreeButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        expandTreeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                expandTreeButtonActionPerformed(evt);
            }
        });
        jToolBar2.add(expandTreeButton);

        org.openide.awt.Mnemonics.setLocalizedText(collapseTreeButton, org.openide.util.NbBundle.getMessage(TreeTopComponent.class, "TreeTopComponent.collapseTreeButton.text_1")); // NOI18N
        collapseTreeButton.setFocusable(false);
        collapseTreeButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        collapseTreeButton.setLabel(org.openide.util.NbBundle.getMessage(TreeTopComponent.class, "TreeTopComponent.collapseTreeButton.text")); // NOI18N
        collapseTreeButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        collapseTreeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                collapseTreeButtonActionPerformed(evt);
            }
        });
        jToolBar2.add(collapseTreeButton);

        searchAntlrTreeTextField.setMaximumSize(new java.awt.Dimension(200, 25));
        searchAntlrTreeTextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                searchAntlrTreeTextFieldKeyPressed(evt);
            }
        });
        jToolBar2.add(searchAntlrTreeTextField);

        treePanel.add(jToolBar2, java.awt.BorderLayout.PAGE_START);

        mainTabbedPane.addTab(org.openide.util.NbBundle.getMessage(TreeTopComponent.class, "TreeTopComponent.treePanel.TabConstraints.tabTitle"), treePanel); // NOI18N

        add(mainTabbedPane, java.awt.BorderLayout.CENTER);
        mainTabbedPane.getAccessibleContext().setAccessibleName(org.openide.util.NbBundle.getMessage(TreeTopComponent.class, "TreeTopComponent.mainTabbedPane.AccessibleContext.accessibleName")); // NOI18N
    }// </editor-fold>//GEN-END:initComponents

    private void refreshGraphvizButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_refreshGraphvizButtonActionPerformed
		try {
			ANTLRv4Lexer lexer;
			JTextComponent jTextComponent = EditorRegistry.focusedComponent();
			if (jTextComponent != null) {
				lexer = new ANTLRv4Lexer(new ANTLRInputStream(jTextComponent.getText()));
			} else {
				TopComponent activeTC = TopComponent.getRegistry().getActivated();
				DataObject dataObject = activeTC.getLookup().lookup(DataObject.class);
				if (dataObject == null || dataObject.getPrimaryFile() == null) {
					ModuleLib.log("refreshGraphvizButtonActionPerformed dataObject is null");
					return;
				}
				lexer = new ANTLRv4Lexer(new ANTLRInputStream(dataObject.getPrimaryFile().getInputStream()));
			}

			CommonTokenStream tokenStream;
			if (NbPreferences.forModule(FileTypeG4VisualElement.class).get("commonTokenStreamIndex", null) == null) {
				tokenStream = new CommonTokenStream(lexer);
			} else {
				tokenStream = new CommonTokenStream(lexer, Integer.parseInt(NbPreferences.forModule(FileTypeG4VisualElement.class).get("commonTokenStreamIndex", null)));
			}
			ANTLRv4Parser parser = new ANTLRv4Parser(tokenStream);
			ANTLRv4Parser.GrammarSpecContext context = parser.grammarSpec();
			ParseTreeWalker walker = new ParseTreeWalker();
			MyANTLRv4ParserListener listener = new MyANTLRv4ParserListener(null, parser);
			walker.walk(listener, context);

			Ast ast = listener.ast;
			AstNode root = ast.getRoot();
			AntlrLib.filterUnwantedSubNodes(root, new String[]{"ruleblock"});
			AntlrLib.removeOneLeafNodes(root);
			AntlrLib.printAst("", root);

			String dot = AntlrLib.exportDot(root, null, searchTextField.getText());

//			System.out.println(dot);
			File dotFile = File.createTempFile("netbeans-antlr", ".dot");
			File dotPngFile = File.createTempFile("netbeans-antlr", ".png");
			FileUtils.writeStringToFile(dotFile, dot, "UTF-8");
//				MutableGraph g = Parser.read(dot);
//				BufferedImage image = Graphviz.fromGraph(g).render(Format.PNG).toImage();
			JProgressBarDialog d = new JProgressBarDialog("Generating dot...", true);
			d.progressBar.setIndeterminate(true);
			d.progressBar.setStringPainted(true);
			d.progressBar.setString("running dot command : " + "dot -Tpng " + dotFile.getAbsolutePath() + " -o " + dotPngFile.getAbsolutePath());
//			if (ModuleLib.isMac()) {
//				CommonLib.runCommand("/opt/local/bin/dot -Tpng " + dotFile.getAbsolutePath() + " -o " + dotPngFile.getAbsolutePath());
//			} else {
			String dotPath = NbPreferences.forModule(FileTypeG4VisualElement.class).get("dotPath", null);
			if (dotPath.contains(" ")) {
				dotPath = "\"" + dotPath + "\"";
			}
			CommonLib.runCommand(dotPath + " -Tpng " + dotFile.getAbsolutePath() + " -o " + dotPngFile.getAbsolutePath());
//			}
			if (dotPngFile.exists()) {
				AntlrLib.addText(dotPngFile);
//			Files.copy(dotPngFile.toPath(), new File("/Users/peter/Desktop/b.png").toPath(), REPLACE_EXISTING);
				ImageIcon icon = new ImageIcon(dotPngFile.getAbsolutePath());
				pngFileName = dotPngFile;
//				icon.getImage().flush();

				preferWidth = graphvizLabel.getWidth() > icon.getIconWidth() ? icon.getIconWidth() : graphvizLabel.getWidth();
				float ratio = ((float) preferWidth) / icon.getIconWidth();
				if (ratio == 0) {
					ratio = 1;
				}
				preferHeight = (int) (icon.getIconHeight() * ratio);
				graphvizLabel.setIcon(resizeImage(icon, preferWidth, preferHeight));

				dotPngFile.deleteOnExit();
			}
			dotFile.delete();
		} catch (Exception ex) {
			ex.printStackTrace();
			ModuleLib.log(ex.getMessage());
		}
    }//GEN-LAST:event_refreshGraphvizButtonActionPerformed

    private void show100ImageButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_show100ImageButtonActionPerformed
		if (pngFileName != null && pngFileName.exists()) {
			ImageIcon icon = new ImageIcon(pngFileName.getAbsolutePath());
			icon.getImage().flush();
			preferWidth = icon.getIconWidth();
			preferHeight = icon.getIconHeight();
			graphvizLabel.setIcon(resizeImage(icon, icon.getIconWidth(), icon.getIconHeight()));
		}
    }//GEN-LAST:event_show100ImageButtonActionPerformed

    private void zoomOutButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_zoomOutButtonActionPerformed
		if (pngFileName != null && pngFileName.exists()) {
			ImageIcon icon = new ImageIcon(pngFileName.getAbsolutePath());
			icon.getImage().flush();
			preferWidth = (int) (((float) preferWidth) * 0.9);
			preferHeight = (int) (((float) preferHeight) * 0.9);
			graphvizLabel.setIcon(resizeImage(icon, preferWidth, preferHeight));
		}
    }//GEN-LAST:event_zoomOutButtonActionPerformed

    private void zoomInButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_zoomInButtonActionPerformed
		if (pngFileName != null && pngFileName.exists()) {
			ImageIcon icon = new ImageIcon(pngFileName.getAbsolutePath());

			icon.getImage().flush();
			preferWidth = (int) (((float) preferWidth) * 1.1);
			preferHeight = (int) (((float) preferHeight) * 1.1);
			graphvizLabel.setIcon(resizeImage(icon, preferWidth, preferHeight));
		}
    }//GEN-LAST:event_zoomInButtonActionPerformed

    private void refreshAntlrTreeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_refreshAntlrTreeButtonActionPerformed
		try {
			ANTLRv4Lexer lexer;
			JTextComponent jTextComponent = EditorRegistry.focusedComponent();
			if (jTextComponent != null) {
				lexer = new ANTLRv4Lexer(new ANTLRInputStream(jTextComponent.getText()));
			} else {
				TopComponent activeTC = TopComponent.getRegistry().getActivated();
				DataObject dataObject = activeTC.getLookup().lookup(DataObject.class);
				if (dataObject == null || dataObject.getPrimaryFile() == null) {
					return;
				}
				lexer = new ANTLRv4Lexer(new ANTLRInputStream(dataObject.getPrimaryFile().getInputStream()));
			}
			CommonTokenStream tokenStream;
			if (NbPreferences.forModule(FileTypeG4VisualElement.class).get("commonTokenStreamIndex", null) == null) {
				tokenStream = new CommonTokenStream(lexer);
			} else {
				tokenStream = new CommonTokenStream(lexer, Integer.parseInt(NbPreferences.forModule(FileTypeG4VisualElement.class).get("commonTokenStreamIndex", null)));
			}
			ANTLRv4Parser parser = new ANTLRv4Parser(tokenStream);
			ANTLRv4Parser.GrammarSpecContext context = parser.grammarSpec();
			ParseTreeWalker walker = new ParseTreeWalker();
			MyANTLRv4ParserListener listener = new MyANTLRv4ParserListener(null, parser);
			walker.walk(listener, context);

			Ast ast = listener.ast;
			AstNode root = ast.getRoot();
			AntlrLib.filterUnwantedSubNodes(root, new String[]{"ruleblock"});
			AntlrLib.removeOneLeafNodes(root);
			rootNode.removeAllChildren();
			AntlrLib.buildTree(root, rootNode);
			CommonLib.expandAll(antlrTree, true);
		} catch (Exception ex) {
			Exceptions.printStackTrace(ex);
		}
    }//GEN-LAST:event_refreshAntlrTreeButtonActionPerformed

    private void expandTreeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_expandTreeButtonActionPerformed
		CommonLib.expandAll(antlrTree, true);
    }//GEN-LAST:event_expandTreeButtonActionPerformed

    private void collapseTreeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_collapseTreeButtonActionPerformed
		CommonLib.expandAll(antlrTree, false);
    }//GEN-LAST:event_collapseTreeButtonActionPerformed

    private void searchAntlrTreeTextFieldKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_searchAntlrTreeTextFieldKeyPressed
//		ModuleLib.log(evt.getKeyCode());
		if (evt.getKeyCode() == 10) {
			treeModel.visibleNode(rootNode, false);
			//treeModel.filterTreeNode(rootNode, searchAntlrTreeTextField.getText());
			//antlrTree.updateUI();
			treeModel.nodeStructureChanged(rootNode);
			CommonLib.expandAll(antlrTree, true);
		}
    }//GEN-LAST:event_searchAntlrTreeTextFieldKeyPressed

    private void graphvizFitWidthButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_graphvizFitWidthButtonActionPerformed
		if (pngFileName != null && pngFileName.exists()) {
			ImageIcon icon = new ImageIcon(pngFileName.getAbsolutePath());
			icon.getImage().flush();
			preferWidth = graphvizLabel.getWidth() > icon.getIconWidth() ? icon.getIconWidth() : graphvizLabel.getWidth();
			float ratio = ((float) preferWidth) / icon.getIconWidth();
			if (ratio == 0) {
				ratio = 1;
			}
			preferHeight = (int) (icon.getIconHeight() * ratio);
			graphvizLabel.setIcon(resizeImage(icon, preferWidth, preferHeight));
		}
    }//GEN-LAST:event_graphvizFitWidthButtonActionPerformed

    private void graphvizFitHeightButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_graphvizFitHeightButtonActionPerformed
		if (pngFileName != null && pngFileName.exists()) {
			ImageIcon icon = new ImageIcon(pngFileName.getAbsolutePath());
			icon.getImage().flush();
			preferHeight = graphvizLabel.getHeight() > icon.getIconHeight() ? icon.getIconHeight() : graphvizLabel.getHeight();
			float ratio = ((float) preferHeight) / icon.getIconHeight();
			if (ratio == 0) {
				ratio = 1;
			}
			preferWidth = (int) (icon.getIconWidth() * ratio);
			graphvizLabel.setIcon(resizeImage(icon, preferWidth, preferHeight));
		}
    }//GEN-LAST:event_graphvizFitHeightButtonActionPerformed

    private void refreshTargetGraphvizButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_refreshTargetGraphvizButtonActionPerformed
		try {
			JTextComponent jTextComponent = EditorRegistry.lastFocusedComponent();
			if (jTextComponent == null) {
				ModuleLib.log("dot: jTextComponent is null");
				return;
			}
			DataObject dataObject = NbEditorUtilities.getDataObject(jTextComponent.getDocument());
			File targetFile = AntlrLib.getTargetFile(dataObject);
			if (targetFile == null) {
				ModuleLib.log("dot: target file is null");
				return;
			}
			String targetFileName = targetFile.getName();

			Tool tool = new Tool();
			ErrorListenerForDot errorListenerForDot = new ErrorListenerForDot();
			GrammarRootAST ast = tool.parseGrammarFromString(jTextComponent.getText());
			if (ast.grammarType == ANTLRParser.COMBINED) {
				Grammar grammar = new Grammar(jTextComponent.getText());

				LexerInterpreter lexer = grammar.createLexerInterpreter(new ANTLRInputStream(new FileInputStream(targetFile)));
				lexer.removeErrorListeners();
				lexer.addErrorListener(errorListenerForDot);

				CommonTokenStream tokenStream;
				if (NbPreferences.forModule(FileTypeG4VisualElement.class).get("commonTokenStreamIndex", null) == null) {
					tokenStream = new CommonTokenStream(lexer);
				} else {
					tokenStream = new CommonTokenStream(lexer, Integer.parseInt(NbPreferences.forModule(FileTypeG4VisualElement.class).get("commonTokenStreamIndex", null)));
				}
				ParserInterpreter parser = grammar.createParserInterpreter(tokenStream);
				parser.removeErrorListeners();
				parser.addErrorListener(errorListenerForDot);
				//parser.getInterpreter().setPredictionMode(PredictionMode.LL);
				String startRule = AntlrLib.getStartRule(dataObject);
				Rule start = grammar.getRule(startRule);
				if (start == null) {
					ModuleLib.log("dot: start rule is null");
					return;
				}
				ParserRuleContext context = parser.parse(start.index);

				ParseTreeWalker walker = new ParseTreeWalker();
				Antlr4GeneralParserListener listener = new Antlr4GeneralParserListener(parser, lexer);
				walker.walk(listener, context);

				Ast astNode = listener.ast;
				AstNode root = astNode.getRoot();
				root.setLabel(targetFile.getName());
				//AntlrLib.filterUnwantedSubNodes(root, new String[]{"ruleblock"});
				//AntlrLib.removeOneLeafNodes(root);
				AntlrLib.printAst("", root);

				this.dot = AntlrLib.exportDot(root, errorListenerForDot.errorInfos, searchTextField.getText());

				File dotFile = File.createTempFile(targetFileName, ".dot");
				File dotPngFile = File.createTempFile(targetFileName, ".png");
				FileUtils.writeStringToFile(dotFile, this.dot, "UTF-8");
//				MutableGraph g = Parser.read(dot);
//				BufferedImage image = Graphviz.fromGraph(g).render(Format.PNG).toImage();
				JProgressBarDialog d = new JProgressBarDialog("Generating dot...", true);
				d.progressBar.setIndeterminate(true);
				d.progressBar.setStringPainted(true);
				d.progressBar.setString("running dot command : " + "dot -Tpng " + dotFile.getAbsolutePath() + " -o " + dotPngFile.getAbsolutePath());
//				if (ModuleLib.isMac()) {
//					CommonLib.runCommand("/opt/local/bin/dot -Tpng " + dotFile.getAbsolutePath() + " -o " + dotPngFile.getAbsolutePath());
//				} else {
				String dotPath = NbPreferences.forModule(FileTypeG4VisualElement.class).get("dotPath", null);
				if (dotPath.contains(" ")) {
					dotPath = "\"" + dotPath + "\"";
				}
				graphvizTargetGraphLabel.setText(CommonLib.runCommand(dotPath + " -Tpng " + dotFile.getAbsolutePath() + " -o " + dotPngFile.getAbsolutePath()));
//				}
				if (dotPngFile.exists()) {
					AntlrLib.addText(dotPngFile);
					//Files.copy(dotPngFile.toPath(), new File("/Users/peter/Desktop/b.png").toPath(), REPLACE_EXISTING);
					ImageIcon icon = new ImageIcon(dotPngFile.getAbsolutePath());
					pngTargetGraphFileName = dotPngFile;
//				icon.getImage().flush();

					if (preferWidth == -1) {
						preferWidth = graphvizLabel.getWidth() > icon.getIconWidth() ? icon.getIconWidth() : graphvizLabel.getWidth();
					}
					float ratio = ((float) preferWidth) / icon.getIconWidth();
					if (ratio == 0) {
						ratio = 1;
					}
					preferHeight = (int) (icon.getIconHeight() * ratio);
					graphvizTargetGraphLabel.setIcon(resizeImage(icon, preferWidth, preferHeight));
					graphvizTargetGraphLabel.setText(null);

					dotPngFile.deleteOnExit();
				}
				dotFile.delete();
			} else {
				File lexerGrammarFile = AntlrLib.getLexerGrammarFile(dataObject);
				if (lexerGrammarFile == null || !lexerGrammarFile.isFile() || !lexerGrammarFile.exists()) {
					ModuleLib.log("refreshTargetJgraphButtonActionPerformed lexerGrammarFile is null");
				} else {
					String strLexer = IOUtils.toString(new FileInputStream(lexerGrammarFile), "utf-8");
					String strParser = jTextComponent.getText();
					LexerGrammar lg = new LexerGrammar(strLexer);
					Grammar grammar = new Grammar(strParser, lg);

					LexerInterpreter lexerInterpreter = lg.createLexerInterpreter(new ANTLRInputStream(new FileInputStream(targetFile)));
					lexerInterpreter.removeErrorListeners();
					lexerInterpreter.addErrorListener(errorListenerForDot);

					CommonTokenStream tokenStream;
					if (NbPreferences.forModule(FileTypeG4VisualElement.class).get("commonTokenStreamIndex", null) == null) {
						tokenStream = new CommonTokenStream(lexerInterpreter);
					} else {
						tokenStream = new CommonTokenStream(lexerInterpreter, Integer.parseInt(NbPreferences.forModule(FileTypeG4VisualElement.class).get("commonTokenStreamIndex", null)));
					}
					System.out.println(grammar.getATN());
					ParserInterpreter parser = grammar.createParserInterpreter(tokenStream);
					parser.removeErrorListeners();
					parser.addErrorListener(errorListenerForDot);
					//parser.getInterpreter().setPredictionMode(PredictionMode.LL);
					String startRule = AntlrLib.getStartRule(dataObject);
					Rule start = grammar.getRule(startRule);
					if (start == null) {
						ModuleLib.log("dot: start rule is null");
						return;
					}
					ParserRuleContext context = parser.parse(start.index);

					ParseTreeWalker walker = new ParseTreeWalker();
					Antlr4GeneralParserListener listener = new Antlr4GeneralParserListener(parser, lexerInterpreter);
					walker.walk(listener, context);

					Ast astNode = listener.ast;
					AstNode root = astNode.getRoot();
					root.setLabel(targetFile.getName());
					//AntlrLib.filterUnwantedSubNodes(root, new String[]{"ruleblock"});
					//AntlrLib.removeOneLeafNodes(root);
					AntlrLib.printAst("", root);

					this.dot = AntlrLib.exportDot(root, errorListenerForDot.errorInfos, searchTextField.getText());

					File dotFile = File.createTempFile(targetFileName, ".dot");
					File dotPngFile = File.createTempFile(targetFileName, ".png");
					FileUtils.writeStringToFile(dotFile, this.dot, "UTF-8");
//				MutableGraph g = Parser.read(dot);
//				BufferedImage image = Graphviz.fromGraph(g).render(Format.PNG).toImage();
					JProgressBarDialog d = new JProgressBarDialog("Generating dot...", true);
					d.progressBar.setIndeterminate(true);
					d.progressBar.setStringPainted(true);
					d.progressBar.setString("running dot command : " + "dot -Tpng " + dotFile.getAbsolutePath() + " -o " + dotPngFile.getAbsolutePath());
//					if (ModuleLib.isMac()) {
//						CommonLib.runCommand("/opt/local/bin/dot -Tpng " + dotFile.getAbsolutePath() + " -o " + dotPngFile.getAbsolutePath());
//					} else {
					String dotPath = NbPreferences.forModule(FileTypeG4VisualElement.class).get("dotPath", null);
					if (dotPath.contains(" ")) {
						dotPath = "\"" + dotPath + "\"";
					}
					graphvizTargetGraphLabel.setText(CommonLib.runCommand(dotPath + " -Tpng " + dotFile.getAbsolutePath() + " -o " + dotPngFile.getAbsolutePath()));
//					}
					//Files.copy(dotPngFile.toPath(), new File("/Users/peter/Desktop/b.png").toPath(), REPLACE_EXISTING);
					if (dotPngFile.exists()) {
						AntlrLib.addText(dotPngFile);
						ImageIcon icon = new ImageIcon(dotPngFile.getAbsolutePath());
						pngTargetGraphFileName = dotPngFile;
//				icon.getImage().flush();
						if (preferWidth == -1) {
							preferWidth = graphvizLabel.getWidth() > icon.getIconWidth() ? icon.getIconWidth() : graphvizLabel.getWidth();
						}
						float ratio = ((float) preferWidth) / icon.getIconWidth();
						if (ratio == 0) {
							ratio = 1;
						}
						preferHeight = (int) (icon.getIconHeight() * ratio);
						graphvizTargetGraphLabel.setIcon(resizeImage(icon, preferWidth, preferHeight));
						graphvizTargetGraphLabel.setText(null);

						dotPngFile.deleteOnExit();
					}
					dotFile.delete();
				}
			}
		} catch (Exception ex) {
			graphvizTargetGraphLabel.setText(ex.getMessage());
			ex.printStackTrace();
			ModuleLib.log(ex.getMessage());
		}
    }//GEN-LAST:event_refreshTargetGraphvizButtonActionPerformed

    private void zoomOutTargetGraphButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_zoomOutTargetGraphButtonActionPerformed
		if (pngTargetGraphFileName != null && pngTargetGraphFileName.exists()) {
			ImageIcon icon = new ImageIcon(pngTargetGraphFileName.getAbsolutePath());
			icon.getImage().flush();
			preferWidth = (int) (((float) preferWidth) * 0.9);
			preferHeight = (int) (((float) preferHeight) * 0.9);
			graphvizTargetGraphLabel.setIcon(resizeImage(icon, preferWidth, preferHeight));
		}
    }//GEN-LAST:event_zoomOutTargetGraphButtonActionPerformed

    private void show100TargetGraphImageButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_show100TargetGraphImageButtonActionPerformed
		if (pngTargetGraphFileName != null && pngTargetGraphFileName.exists()) {
			ImageIcon icon = new ImageIcon(pngTargetGraphFileName.getAbsolutePath());
			icon.getImage().flush();
			preferWidth = icon.getIconWidth();
			preferHeight = icon.getIconHeight();
			graphvizTargetGraphLabel.setIcon(resizeImage(icon, preferWidth, preferHeight));
		}
    }//GEN-LAST:event_show100TargetGraphImageButtonActionPerformed

    private void zoomInTargetGraphButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_zoomInTargetGraphButtonActionPerformed
		if (pngTargetGraphFileName != null && pngTargetGraphFileName.exists()) {
			ImageIcon icon = new ImageIcon(pngTargetGraphFileName.getAbsolutePath());

			icon.getImage().flush();
			preferWidth = (int) (((float) preferWidth) * 1.1);
			preferHeight = (int) (((float) preferHeight) * 1.1);
			graphvizTargetGraphLabel.setIcon(resizeImage(icon, preferWidth, preferHeight));
		}
    }//GEN-LAST:event_zoomInTargetGraphButtonActionPerformed

    private void zoomOutJGraphButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_zoomOutJGraphButtonActionPerformed
		if (graphComponent != null) {
			graphComponent.zoomOut();
		}
    }//GEN-LAST:event_zoomOutJGraphButtonActionPerformed

    private void show100ImageJGraphButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_show100ImageJGraphButtonActionPerformed
		if (graphComponent != null) {
			graphComponent.zoomActual();
		}
    }//GEN-LAST:event_show100ImageJGraphButtonActionPerformed

    private void zoomInJGraphButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_zoomInJGraphButtonActionPerformed
		if (graphComponent != null) {
			graphComponent.zoomIn();
		}
    }//GEN-LAST:event_zoomInJGraphButtonActionPerformed

    private void layoutButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_layoutButtonActionPerformed
		final mxGraph graph = graphComponent.getGraph();
		Object cell = graph.getSelectionCell();

		if (cell == null || graph.getModel().getChildCount(cell) == 0) {
			cell = graph.getDefaultParent();
		}
		graph.getModel().beginUpdate();

		String str;
		if (layoutButton.getEventSource() == null) {
			str = "Hierarchical Layout";
		} else {
			str = ((JMenuItem) layoutButton.getEventSource()).getText();
		}
		if (str.equals("Hierarchical Layout")) {
			mxHierarchicalLayout layout = new mxHierarchicalLayout(graph);
			layout.execute(cell);
		} else if (str.equals("Circle Layout")) {
			mxCircleLayout layout = new mxCircleLayout(graph);
			layout.setDisableEdgeStyle(false);
			layout.execute(cell);
		} else if (str.equals("Organic Layout")) {
			mxOrganicLayout layout = new mxOrganicLayout(graph);
			layout.execute(cell);
		} else if (str.equals("Compact Tree Layout")) {
			mxCompactTreeLayout layout = new mxCompactTreeLayout(graph, false);
			layout.execute(cell);
		} else if (str.equals("Edge Label Layout")) {
			mxEdgeLabelLayout layout = new mxEdgeLabelLayout(graph);
			layout.execute(cell);
		} else if (str.equals("Fast Organic Layout")) {
			mxFastOrganicLayout layout = new mxFastOrganicLayout(graph);
			layout.execute(cell);
		} else if (str.equals("Orthogonal Layout")) {
			mxOrthogonalLayout layout = new mxOrthogonalLayout(graph);
			layout.execute(cell);
		} else if (str.equals("Parallel Edge Layout")) {
			mxParallelEdgeLayout layout = new mxParallelEdgeLayout(graph);
			layout.execute(cell);
		} else if (str.equals("Stack Layout")) {
			mxStackLayout layout = new mxStackLayout(graph);
			layout.execute(cell);
		} else {
			System.out.println("no this layout");
		}

		mxMorphing morph = new mxMorphing(graphComponent, 20, 1.2, 20);
		morph.addListener(mxEvent.DONE, new mxEventSource.mxIEventListener() {
			public void invoke(Object sender, mxEventObject evt) {
				graph.getModel().endUpdate();
			}
		});

		morph.startAnimation();
    }//GEN-LAST:event_layoutButtonActionPerformed

	public void refreshTargetJgraphButtonActionPerformed() {
		refreshTargetJgraphButtonActionPerformed(null);
	}

    private void refreshTargetJgraphButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_refreshTargetJgraphButtonActionPerformed
		try {
			JTextComponent jTextComponent = EditorRegistry.lastFocusedComponent();
			if (jTextComponent == null) {
				ModuleLib.log("jTextComponent is null");
				return;
			}
			Document doc = jTextComponent.getDocument();
			DataObject dataObject = NbEditorUtilities.getDataObject(doc);
			File targetFile = AntlrLib.getTargetFile(dataObject);
			if (targetFile == null) {
				ModuleLib.log("targetFile is null, " + dataObject.getPrimaryFile().getPath());
				return;
			}
			Tool tool = new Tool();
			GrammarRootAST ast = tool.parseGrammarFromString(jTextComponent.getText());
			if (ast.grammarType == ANTLRParser.COMBINED) {
				Grammar grammar = new Grammar(jTextComponent.getText());

				LexerInterpreter lexer = grammar.createLexerInterpreter(new ANTLRInputStream(new FileInputStream(targetFile)));
				lexer.removeErrorListeners();

				CommonTokenStream tokenStream;
				if (NbPreferences.forModule(FileTypeG4VisualElement.class).get("commonTokenStreamIndex", null) == null) {
					tokenStream = new CommonTokenStream(lexer);
				} else {
					tokenStream = new CommonTokenStream(lexer, Integer.parseInt(NbPreferences.forModule(FileTypeG4VisualElement.class).get("commonTokenStreamIndex", null)));
				}
				ParserInterpreter parser = grammar.createParserInterpreter(tokenStream);
				parser.removeErrorListeners();
				//parser.getInterpreter().setPredictionMode(PredictionMode.LL);
				String startRule = AntlrLib.getStartRule(dataObject);
				Rule start = grammar.getRule(startRule);
				if (start == null) {
					ModuleLib.log("refreshTargetJgraphButtonActionPerformed start rule is null");
					return;
				}
				ParserRuleContext context = parser.parse(start.index);

				ParseTreeWalker walker = new ParseTreeWalker();
				Antlr4GeneralParserListener listener = new Antlr4GeneralParserListener(parser, lexer);
				walker.walk(listener, context);

				Ast astNode = listener.ast;
				AstNode root = astNode.getRoot();
				root.setLabel(targetFile.getName());
				//AntlrLib.filterUnwantedSubNodes(root, new String[]{"ruleblock"});
				//AntlrLib.removeOneLeafNodes(root);
				//AntlrLib.printAst("", root);

				buildJGraph(root);
				layoutButtonActionPerformed(null);
			} else {
				File lexerGrammarFile = AntlrLib.getLexerGrammarFile(dataObject);
				if (lexerGrammarFile == null) {
					ModuleLib.log("refreshTargetJgraphButtonActionPerformed lexerGrammarFile is null");
				} else {
					String strLexer = IOUtils.toString(new FileInputStream(lexerGrammarFile), "utf-8");
					String strParser = jTextComponent.getText();
					LexerGrammar lg = new LexerGrammar(strLexer);
					Grammar grammar = new Grammar(strParser, lg);

					LexerInterpreter lexerInterpreter = lg.createLexerInterpreter(new ANTLRInputStream(new FileInputStream(targetFile)));
					lexerInterpreter.removeErrorListeners();

					CommonTokenStream tokenStream;
					if (NbPreferences.forModule(FileTypeG4VisualElement.class).get("commonTokenStreamIndex", null) == null) {
						tokenStream = new CommonTokenStream(lexerInterpreter);
					} else {
						tokenStream = new CommonTokenStream(lexerInterpreter, Integer.parseInt(NbPreferences.forModule(FileTypeG4VisualElement.class).get("commonTokenStreamIndex", null)));
					}
					ParserInterpreter parser = grammar.createParserInterpreter(tokenStream);
					parser.removeErrorListeners();

					//parser.getInterpreter().setPredictionMode(PredictionMode.LL);
					String startRule = AntlrLib.getStartRule(dataObject);
					Rule start = grammar.getRule(startRule);
					if (start == null) {
						ModuleLib.log("refreshTargetJgraphButtonActionPerformed start rule is null");
						return;
					}
					ParserRuleContext context = parser.parse(start.index);

					ParseTreeWalker walker = new ParseTreeWalker();
					Antlr4GeneralParserListener listener = new Antlr4GeneralParserListener(parser, lexerInterpreter);
					walker.walk(listener, context);

					Ast astNode = listener.ast;
					AstNode root = astNode.getRoot();
					root.setLabel(targetFile.getName());
					//AntlrLib.filterUnwantedSubNodes(root, new String[]{"ruleblock"});
					//AntlrLib.removeOneLeafNodes(root);
					//AntlrLib.printAst("", root);

					buildJGraph(root);
					layoutButtonActionPerformed(null);
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			ModuleLib.log("refreshTargetJgraphButtonActionPerformed exception " + ex.getMessage());
		}
    }//GEN-LAST:event_refreshTargetJgraphButtonActionPerformed

    private void tokenTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tokenTableMouseClicked
		try {
			JTextComponent currentTextComponent = EditorRegistry.lastFocusedComponent();
			if (currentTextComponent == null) {
				return;
			}
			Document doc = currentTextComponent.getDocument();
			DataObject dataObject = NbEditorUtilities.getDataObject(doc);
			File targetFile = AntlrLib.getTargetFile(dataObject);
			if (targetFile == null) {
				return;
			}
			FileObject fileObject = FileUtil.toFileObject(targetFile);
			DataObject targetDataObject = DataObject.find(fileObject);
			EditorCookie ecA = targetDataObject.getLookup().lookup(EditorCookie.class);
			Document targetDoc = ecA.getDocument();
			if (targetDoc == null) {
				return;
			}
			JTextComponent targetTextComponent = EditorRegistry.findComponent(targetDoc);

			Token token = tokenTableModel.getToken(tokenTable.getSelectedRow(), tokenTable.getSelectedColumn());

			CaretListenerForTargetTable.enable = false;
			int countStart = StringUtils.countMatches(targetTextComponent.getText().substring(0, token.getStartIndex()), "\r");
			int countEnd = StringUtils.countMatches(targetTextComponent.getText().substring(0, token.getStopIndex()), "\r");
			targetTextComponent.setSelectionStart(token.getStartIndex() - countStart);
			targetTextComponent.setSelectionEnd(token.getStopIndex() + 1 - countEnd);

//			JTextArea temp = new JTextArea(targetTextComponent.getText());
//			System.out.println("token.getLine()=" + token.getLine());
//			System.out.println("token.getStartIndex()=" + token.getStartIndex());
//			int startIndex = temp.getLineStartOffset(token.getLine() - 1) + token.getCharPositionInLine();
//			int endIndex = startIndex + (token.getStopIndex() - token.getStartIndex()) + 1;
//			targetTextComponent.setSelectionStart(startIndex);
//			targetTextComponent.setSelectionEnd(endIndex);
//			targetTextComponent.setSelectionStart(token.getStartIndex());
//			targetTextComponent.setSelectionEnd(token.getStopIndex() + 1);
			CaretListenerForTargetTable.enable = true;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
    }//GEN-LAST:event_tokenTableMouseClicked

    private void clearJGraphButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clearJGraphButtonActionPerformed
		graph.removeCells(graph.getChildVertices(graph.getDefaultParent()), true);
		graph.refresh();
    }//GEN-LAST:event_clearJGraphButtonActionPerformed

    private void copyDotButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_copyDotButtonActionPerformed
		StringSelection stringSelection = new StringSelection(dot);
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		clipboard.setContents(stringSelection, null);
    }//GEN-LAST:event_copyDotButtonActionPerformed

    private void graphvizTargetGraphLabelMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_graphvizTargetGraphLabelMouseReleased
		setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
    }//GEN-LAST:event_graphvizTargetGraphLabelMouseReleased

    private void graphvizTargetGraphLabelMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_graphvizTargetGraphLabelMousePressed
		if (System.getProperty("os.name").startsWith("Mac OS X")) {
			setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		} else {
			setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
		}
		xDiff = evt.getX();
		yDiff = evt.getY();
    }//GEN-LAST:event_graphvizTargetGraphLabelMousePressed

    private void graphvizTargetGraphLabelMouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_graphvizTargetGraphLabelMouseDragged
		c = graphvizTargetGraphLabel.getParent();
		if (c instanceof JViewport) {
			JViewport jv = (JViewport) c;
			Point p = jv.getViewPosition();
			int newX = p.x - (evt.getX() - xDiff);
			int newY = p.y - (evt.getY() - yDiff);

			int maxX = graphvizTargetGraphLabel.getWidth() - jv.getWidth();
			int maxY = graphvizTargetGraphLabel.getHeight() - jv.getHeight();
			if (newX < 0) {
				newX = 0;
			}
			if (newX > maxX) {
				newX = maxX;
			}
			if (newY < 0) {
				newY = 0;
			}
			if (newY > maxY) {
				newY = maxY;
			}

			jv.setViewPosition(new Point(newX, newY));
		}
    }//GEN-LAST:event_graphvizTargetGraphLabelMouseDragged

    private void clearDotButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clearDotButtonActionPerformed
		graphvizTargetGraphLabel.setIcon(null);
    }//GEN-LAST:event_clearDotButtonActionPerformed

    private void show75TargetGraphImageButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_show75TargetGraphImageButtonActionPerformed
		if (pngTargetGraphFileName != null && pngTargetGraphFileName.exists()) {
			ImageIcon icon = new ImageIcon(pngTargetGraphFileName.getAbsolutePath());
			icon.getImage().flush();
			preferWidth = (int) (icon.getIconWidth() * 0.75);
			preferHeight = (int) (icon.getIconHeight() * 0.75);
			graphvizTargetGraphLabel.setIcon(resizeImage(icon, preferWidth, preferHeight));
		}
    }//GEN-LAST:event_show75TargetGraphImageButtonActionPerformed

    private void saveDotButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveDotButtonActionPerformed
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setDialogTitle("Specify a dot file to save");
		fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("Dot", "dot"));
		fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("Image", "png"));
		int userSelection = fileChooser.showSaveDialog(this);
		if (userSelection == JFileChooser.APPROVE_OPTION) {
			try {
				File file = fileChooser.getSelectedFile();
				if (file.getName().endsWith(".dot")) {
					FileUtils.writeStringToFile(file, dot, "utf-8");
				} else if (file.getName().endsWith(".png")) {
					Files.copy(pngTargetGraphFileName.toPath(), file.toPath());
				}
			} catch (IOException ex) {
				Exceptions.printStackTrace(ex);
			}
		}
    }//GEN-LAST:event_saveDotButtonActionPerformed

    private void searchTextFieldKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_searchTextFieldKeyPressed
		if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
			refreshGraphvizButtonActionPerformed(null); // this not work since focus not on editor
		}
    }//GEN-LAST:event_searchTextFieldKeyPressed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTree antlrTree;
    private javax.swing.JButton clearDotButton;
    private javax.swing.JButton clearJGraphButton;
    private javax.swing.JButton collapseTreeButton;
    public static javax.swing.JEditorPane compileEditorPane;
    private javax.swing.JPanel compilePanel;
    private javax.swing.JButton copyDotButton;
    private javax.swing.JButton expandTreeButton;
    private javax.swing.JButton graphvizFitHeightButton;
    private javax.swing.JButton graphvizFitWidthButton;
    private javax.swing.JLabel graphvizLabel;
    private javax.swing.JPanel graphvizPanel;
    private javax.swing.JScrollPane graphvizScrollPane;
    private javax.swing.JScrollPane graphvizScrollPane1;
    private javax.swing.JLabel graphvizTargetGraphLabel;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JToolBar jToolBar2;
    private javax.swing.JToolBar jToolBar3;
    private javax.swing.JToolBar jToolBar4;
    private hk.quantr.javalib.swing.advancedswing.jdropdownbutton.JDropDownButton layoutButton;
    private javax.swing.JTabbedPane mainTabbedPane;
    private javax.swing.JButton refreshAntlrTreeButton;
    private javax.swing.JButton refreshGraphvizButton;
    private javax.swing.JButton refreshTargetGraphvizButton;
    private javax.swing.JButton refreshTargetJgraphButton;
    private javax.swing.JButton saveDotButton;
    private hk.quantr.javalib.swing.advancedswing.searchtextfield.JSearchTextField searchAntlrTreeTextField;
    private javax.swing.JTextField searchTextField;
    private javax.swing.JButton show100ImageButton;
    private javax.swing.JButton show100ImageJGraphButton;
    private javax.swing.JButton show100TargetGraphImageButton;
    private javax.swing.JButton show75TargetGraphImageButton;
    private javax.swing.JPanel targetGraphPanel;
    private javax.swing.JPanel targetJGraphPanel;
    private javax.swing.JPanel tokenPanel;
    public javax.swing.JTable tokenTable;
    private javax.swing.JPanel treePanel;
    private javax.swing.JButton zoomInButton;
    private javax.swing.JButton zoomInJGraphButton;
    private javax.swing.JButton zoomInTargetGraphButton;
    private javax.swing.JButton zoomOutButton;
    private javax.swing.JButton zoomOutJGraphButton;
    private javax.swing.JButton zoomOutTargetGraphButton;
    // End of variables declaration//GEN-END:variables

	ImageIcon resizeImage(ImageIcon icon, int width, int height) {
		return new ImageIcon(icon.getImage().getScaledInstance(width, height, Image.SCALE_DEFAULT));
	}

	private void buildJGraph(AstNode root) {
		graph.removeCells(graph.getChildVertices(graph.getDefaultParent()), true);
		buildJGraphNode(root, null);
		graph.setCellsDisconnectable(false);
		graph.getModel().beginUpdate();
		mxMorphing morph = new mxMorphing(graphComponent, 20, 1.2, 20);
		morph.addListener(mxEvent.DONE, new mxEventSource.mxIEventListener() {
			public void invoke(Object sender, mxEventObject evt) {
				graph.getModel().endUpdate();
			}
		});

		morph.startAnimation();
	}

	private void buildJGraphNode(AstNode node, mxCell mxNode) {
		mxCell newNode = (mxCell) graph.insertVertex(null, null, node, 40, 40, 300, 30);
		if (mxNode != null) {
			graph.insertEdge(null, null, "", mxNode, newNode, mxConstants.STYLE_STROKECOLOR + "=#000000;edgeStyle=elbowEdgeStyle;");
		}

		for (AstNode nn : node.getChildren()) {
			buildJGraphNode(nn, newNode);
		}
	}

}
