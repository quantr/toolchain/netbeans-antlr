package hk.quantr.netbeans.antlr.syntax.antlr4;

import hk.quantr.netbeans.antlr.ModuleLib;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.apache.commons.lang3.StringUtils;
import org.netbeans.api.editor.EditorRegistry;
import org.netbeans.api.editor.mimelookup.MimeRegistration;
import org.netbeans.lib.editor.hyperlink.spi.HyperlinkProvider;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
@MimeRegistration(mimeType = "text/x-g4", service = HyperlinkProvider.class)
public class Antlr4HyperlinkProvider implements HyperlinkProvider {

	@Override
	public boolean isHyperlinkPoint(Document dcmnt, int i) {
//		ModuleLib.log("isHyperlinkPoint " + i);
		for (TokenDocumentLocation tokenDocumentLocation : MyANTLRv4ParserListener.ruleTokenDocumentLocationSources) {
//			ModuleLib.log(i + " ===> " + tokenDocumentLocation);
			if (MyANTLRv4ParserListener.containTarget(tokenDocumentLocation.text) && tokenDocumentLocation.start <= i && i <= tokenDocumentLocation.stop) {
				ModuleLib.log("good ===> " + tokenDocumentLocation);
				return true;
			} else {
				//System.out.println("shit ===> " + tokenDocumentLocation);
			}
		}
		return false;
	}

	@Override
	public int[] getHyperlinkSpan(Document dcmnt, int i) {
		ModuleLib.log("getHyperlinkSpan " + i);
		for (TokenDocumentLocation tokenDocumentLocation : MyANTLRv4ParserListener.ruleTokenDocumentLocationSources) {
			if (tokenDocumentLocation.start <= i && i <= tokenDocumentLocation.stop) {
				return new int[]{tokenDocumentLocation.start, tokenDocumentLocation.stop + 1};
			}
		}
		return null;
	}

	@Override
	public void performClickAction(Document dcmnt, int i) {
		ModuleLib.log("performClickAction " + i);
		String text = null;
		for (TokenDocumentLocation tokenDocumentLocation : MyANTLRv4ParserListener.ruleTokenDocumentLocationSources) {
//			System.out.println("tokenDocumentLocation=" + tokenDocumentLocation);
			if (MyANTLRv4ParserListener.containTarget(tokenDocumentLocation.text)) {
				if (tokenDocumentLocation.start <= i && i <= tokenDocumentLocation.stop) {
					text = tokenDocumentLocation.text;
					break;
				}
			}
		}
		if (text != null) {
			JTextComponent jTextComponent = EditorRegistry.lastFocusedComponent();
			for (TokenDocumentLocation tokenDocumentLocation : MyANTLRv4ParserListener.ruleTokenDocumentLocationTargets) {
				if (tokenDocumentLocation.text.equals(text)) {
					int count = StringUtils.countMatches(jTextComponent.getText().substring(0, tokenDocumentLocation.start), "\r\n");
					jTextComponent.setCaretPosition(tokenDocumentLocation.start/* - count*/);
					return;
				}
			}
		}
	}

}
