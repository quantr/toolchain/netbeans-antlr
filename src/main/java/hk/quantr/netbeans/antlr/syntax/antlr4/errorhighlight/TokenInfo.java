// License : Apache License Version 2.0  https://www.apache.org/licenses/LICENSE-2.0
package hk.quantr.netbeans.antlr.syntax.antlr4.errorhighlight;

/**
 *
 * @author Peter <mcheung63@hotmail.com>
 */
public class TokenInfo {

	public int lineNo;
	public String symbolName;
	public int start;
	public int stop;

	public TokenInfo(int lineNo, String symbolName, int start, int stop) {
		this.lineNo = lineNo;
		this.symbolName = symbolName;
		this.start = start;
		this.stop = stop;
	}

}
