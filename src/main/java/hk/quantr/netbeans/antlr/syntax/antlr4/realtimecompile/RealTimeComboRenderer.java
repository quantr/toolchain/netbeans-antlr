package hk.quantr.netbeans.antlr.syntax.antlr4.realtimecompile;

import java.awt.Component;
import java.io.File;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class RealTimeComboRenderer implements ListCellRenderer {

	DefaultListCellRenderer defaultRenderer = new DefaultListCellRenderer();

	@Override
	public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
		JLabel label = (JLabel) defaultRenderer.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
//		label.setOpaque(true);
//		label.setBackground(Color.red);
		File file = (File) value;
		if (file == null) {
			label.setText(null);
		} else if (!file.exists()) {
			label.setText(" ");
		} else {
			label.setText(file.getName() + ", " + file.getParentFile().getAbsolutePath());
		}
		return label;
	}

}
