package hk.quantr.netbeans.antlr.syntax.antlr4.errorhighlight;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class ErrorInfo {

	public int offsetStart;
	public int offsetEnd;
	public String message;

	public ErrorInfo(int offsetStart, int offsetEnd, String message) {
		this.offsetStart = offsetStart;
		this.offsetEnd = offsetEnd;
		this.message = message;
	}

	@Override
	public String toString() {
		return "Error " + offsetStart + ":" + offsetEnd + "\t" + message;
	}
}
