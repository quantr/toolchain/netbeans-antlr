package hk.quantr.netbeans.antlr.syntax.antlr4;

import hk.quantr.netbeans.antlr.ModuleLib;
import org.antlr.parser.ANTLRv4Lexer;
import org.netbeans.api.lexer.Token;
import org.netbeans.spi.lexer.Lexer;
import org.netbeans.spi.lexer.LexerRestartInfo;

public class Antlr4EditorLexer implements Lexer<Antlr4TokenId> {

	private LexerRestartInfo<Antlr4TokenId> info;
	private ANTLRv4Lexer lexer;

	public Antlr4EditorLexer(LexerRestartInfo<Antlr4TokenId> info) {
		this.info = info;
		Antlr4CharStream charStream = new Antlr4CharStream(info.input(), "Antlr4Editor");
		lexer = new ANTLRv4Lexer(charStream);
	}

	@Override
	public Token<Antlr4TokenId> nextToken() {
		org.antlr.v4.runtime.Token token = lexer.nextToken();
		if (token.getType() != ANTLRv4Lexer.EOF) {
			String tokenName = Antlr4LanguageHierarchy.getToken(token.getType()).name();

			if (ModuleLib.isDarkTheme()) {
				Antlr4TokenId darkTokenId = Antlr4LanguageHierarchy.getToken("DARK_" + tokenName);
				return info.tokenFactory().createToken(darkTokenId);
			} else {
				if (tokenName.equals("ACTION_CONTENT")) {
					Antlr4TokenId tokenId = Antlr4LanguageHierarchy.getToken("ACTION_CONTENT_FUCK");
					return info.tokenFactory().createToken(tokenId);
				} else {
					Antlr4TokenId tokenId = Antlr4LanguageHierarchy.getToken(tokenName);
					return info.tokenFactory().createToken(tokenId);
				}
			}
		}
		return null;
	}

	@Override
	public Object state() {
		return null;
	}

	@Override
	public void release() {
	}

}
