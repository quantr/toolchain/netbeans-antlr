// License : Apache License Version 2.0  https://www.apache.org/licenses/LICENSE-2.0
package hk.quantr.netbeans.antlr.syntax.antlr4.realtimecompile;

import hk.quantr.netbeans.antlr.TokenTableCellRenderer;
import hk.quantr.netbeans.antlr.Antlr4TokenTableModel;
import hk.quantr.netbeans.antlr.TreeTopComponent;
import hk.quantr.javalib.CommonLib;
import java.util.ArrayList;
import javax.swing.JTable;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.JTextComponent;
import org.antlr.v4.runtime.Token;
import org.openide.util.Exceptions;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class CaretListenerForTargetTable implements CaretListener {

	public static boolean enable = true;

	@Override
	public void caretUpdate(CaretEvent e) {
		try {
			if (!enable) {
				return;
			}
			if (TreeTopComponent.treeTopComponent == null || TreeTopComponent.treeTopComponent.tokenTable == null) {
				return;
			}
			JTable table = TreeTopComponent.treeTopComponent.tokenTable;
			if (table == null) {
				return;
			}
			Antlr4TokenTableModel model = (Antlr4TokenTableModel) table.getModel();
			TokenTableCellRenderer renderer = (TokenTableCellRenderer) table.getDefaultRenderer(Object.class);
			JTextComponent textComponent = (JTextComponent) e.getSource();
			int dot = e.getDot();
			int line = getLineOfOffset(textComponent, dot);
			if (line < table.getRowCount()) {
				ArrayList<Token> tokens = model.tokens.get(line + 1);
				int column = 2;
				if (tokens != null) {
					for (Token token : tokens) {
//						ModuleLib.log("dot " + token.start + " <= " + dot + " <= " + token.stop);
						if (token.getStartIndex() <= dot && dot <= token.getStopIndex()) {
							break;
						}
						column++;
					}
				}
//				ModuleLib.log("column=" + column);
				if (column < table.getColumnCount()) {
					table.setColumnSelectionInterval(column, column);
				}
				renderer.selectedRow = line;
				table.setRowSelectionInterval(line, line);
				//ModuleLib.log("scrollTableToVisible=" + line);
				CommonLib.scrollTableToVisible(table, line, 0);
			}
		} catch (BadLocationException ex) {
			Exceptions.printStackTrace(ex);
		}
	}

	static int getLineOfOffset(JTextComponent comp, int offset) throws BadLocationException {
		Document doc = comp.getDocument();
		if (offset < 0) {
			throw new BadLocationException("Can't translate offset to line", -1);
		} else if (offset > doc.getLength()) {
			throw new BadLocationException("Can't translate offset to line", doc.getLength() + 1);
		} else {
			Element map = doc.getDefaultRootElement();
			return map.getElementIndex(offset);
		}
	}
}
