package hk.quantr.netbeans.antlr.syntax.antlr4.errorhighlight;

import hk.quantr.netbeans.antlr.FileTypeG4VisualElement;
import hk.quantr.netbeans.antlr.ModuleLib;
import hk.quantr.netbeans.antlr.syntax.antlr4.MyANTLRv4ParserListener;
import java.util.BitSet;
import javax.swing.event.ChangeListener;
import org.antlr.parser.ANTLRv4Lexer;
import org.antlr.parser.ANTLRv4Parser;
import org.antlr.v4.runtime.ANTLRErrorListener;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.atn.ATNConfigSet;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Task;
import org.netbeans.modules.parsing.spi.ParseException;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.SourceModificationEvent;
import org.openide.util.NbPreferences;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class Antlr4Parser extends Parser {

	private Snapshot snapshot;
	public int embeddedOffset;

	@Override
	public void parse(Snapshot snapshot, Task task, SourceModificationEvent sme) throws ParseException {
		ModuleLib.log("Antlr4Parser::parse()");
		this.snapshot = snapshot;
		ANTLRv4Lexer lexer = new ANTLRv4Lexer(new ANTLRInputStream(snapshot.getText().toString()));
		CommonTokenStream tokenStream;
				if (NbPreferences.forModule(FileTypeG4VisualElement.class).get("commonTokenStreamIndex", null) == null) {
					tokenStream = new CommonTokenStream(lexer);
				} else {
					tokenStream = new CommonTokenStream(lexer, Integer.parseInt(NbPreferences.forModule(FileTypeG4VisualElement.class).get("commonTokenStreamIndex", null)));
				}
		ANTLRv4Parser parser = new ANTLRv4Parser(tokenStream);
		ErrorHighlightingTask.errorInfos.clear();
		parser.addErrorListener(new ANTLRErrorListener() {
			@Override
			public void syntaxError(Recognizer<?, ?> rcgnzr, Object offendingSymbol, int lineNumber, int charOffsetFromLine, String message, RecognitionException re) {
				ModuleLib.log("Antlr4Parser::syntaxError");
				Token offendingToken = (Token) offendingSymbol;
				int start = offendingToken.getStartIndex() + snapshot.getOriginalOffset(0);
				int stop = offendingToken.getStopIndex() + snapshot.getOriginalOffset(0);
				//ModuleLib.log("syntaxError " + rcgnzr + ", " + lineNumber + ", " + charOffsetFromLine + ", " + start + ", " + stop + ", " + message + ", " + re);
				//ModuleLib.log("\t\t " + rcgnzr + ", " + offendingToken.getStartIndex() + ", " + offendingToken.getStopIndex());

				ErrorHighlightingTask.errorInfos.add(new ErrorInfo(start, stop, message));
			}

			@Override
			public void reportAmbiguity(org.antlr.v4.runtime.Parser parser, DFA dfa, int i, int i1, boolean bln, BitSet bitset, ATNConfigSet atncs) {
				ModuleLib.log("Antlr4Parser::reportAmbiguity");
			}

			@Override
			public void reportAttemptingFullContext(org.antlr.v4.runtime.Parser parser, DFA dfa, int i, int i1, BitSet bitset, ATNConfigSet atncs) {
				ModuleLib.log("Antlr4Parser::reportAttemptingFullContext");
			}

			@Override
			public void reportContextSensitivity(org.antlr.v4.runtime.Parser parser, DFA dfa, int i, int i1, int i2, ATNConfigSet atncs) {
				ModuleLib.log("Antlr4Parser::reportContextSensitivity");
			}
		});
		ANTLRv4Parser.GrammarSpecContext context = parser.grammarSpec();
		ParseTreeWalker walker = new ParseTreeWalker();
		MyANTLRv4ParserListener listener = new MyANTLRv4ParserListener("", parser);
		walker.walk(listener, context);
	}

	@Override
	public Result getResult(Task task) throws ParseException {
		return new Antlr4ParseResult(snapshot);
	}

	@Override
	public void addChangeListener(ChangeListener cl) {
	}

	@Override
	public void removeChangeListener(ChangeListener cl) {
	}
}
