package hk.quantr.netbeans.antlr.syntax.antlr4;

import hk.quantr.netbeans.antlr.ModuleLib;
import hk.quantr.netbeans.antlr.TreeTopComponent;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.Predicate;
import org.antlr.parser.ANTLRv4ParserBaseListener;
import org.antlr.v4.runtime.LexerInterpreter;
import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.TerminalNode;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class Antlr4GeneralParserListener extends ANTLRv4ParserBaseListener {

	LexerInterpreter lexer;
	Parser parser;
	public Ast ast = null;
	private final Map<String, Integer> rmap = new HashMap<>();
	Predicate<String> filter = x -> !x.isEmpty();
	AstNode parentNode = null;
	String content;

	public Antlr4GeneralParserListener(Parser parser, LexerInterpreter lexer) {
		this.lexer = lexer;
		this.parser = parser;
		ast = new Ast("root", "root");
		content = lexer.getInputStream().toString();
		parentNode = ast.getRoot();
		rmap.putAll(parser.getRuleIndexMap());
//		if (TreeTopComponent.treeTopComponent != null) {
//			Antlr4TokenTableModel parserTableModel = TreeTopComponent.treeTopComponent.parserTableModel;
//			if (parserTableModel != null) {
//				parserTableModel.tokens.clear();
//			}
//		}
	}

	@Override
	public void enterEveryRule(ParserRuleContext ctx) {
		try {
			String ruleName = getRuleByKey(ctx.getRuleIndex());
			//System.out.println(ruleName+" = "+ctx.getText());
			String thisContent = content.substring(ctx.getStart().getStartIndex(), ctx.getStop().getStopIndex() + 1);
//			ModuleLib.log(ruleName + " = " + ctx.getStart().getStartIndex() + " , " + ctx.getStop().getStopIndex() + " = " + thisContent + " = " + ctx.getText());

			String text = ctx.getText();
			Token s = ctx.getStart();
			Token e = ctx.getStop();
			AstNode n = ast.newNode(parentNode, ruleName, thisContent, s != null ? s.getStartIndex() : 0, e != null ? e.getStopIndex() : 0);
			parentNode.addChild(n);
			parentNode = n;
		} catch (Exception ex) {

		}
	}

	@Override
	public void exitEveryRule(ParserRuleContext ctx) {
		//String ruleName = getRuleByKey(ctx.getRuleIndex());(ParserRuleContext ctx) {
		if (parentNode != null) {
			parentNode = parentNode.getParent();
		}
	}

	public String getRuleByKey(int key) {
		return rmap.entrySet().stream().filter(e -> Objects.equals(e.getValue(), key)).map(Map.Entry::getKey).findFirst().orElse(null);
	}

	@Override
	public void visitTerminal(TerminalNode tn) {
		try {
			String temp = parser.getVocabulary().getSymbolicName(tn.getSymbol().getType());
//			ModuleLib.log("visitTerminal = " + temp + " - " + tn.getText());
			if (TreeTopComponent.treeTopComponent == null) {
				return;
			}
			AstNode n = ast.newNode(parentNode, temp, tn.getText(), tn.getSymbol() != null ? tn.getSymbol().getStartIndex() : 0, tn.getSymbol() != null ? tn.getSymbol().getStopIndex() : 0);
			parentNode.addChild(n);

//		Antlr4TokenTableModel tokenTableModel = TreeTopComponent.treeTopComponent.parserTableModel;
//		if (tokenTableModel != null) {
//			ArrayList<TokenInfo> list = tokenTableModel.tokens.get(tn.getSymbol().getLine());
//			if (list == null) {
//				list = new ArrayList<>();
//			}
////			if (temp == null) {
////				temp = tn.getText();
////			}
//			list.add(new TokenInfo(tn.getSymbol().getLine(), temp, tn.getSymbol().getStartIndex(), tn.getSymbol().getStopIndex()));
//
//			tokenTableModel.tokens.put(tn.getSymbol().getLine(), list);
////			ModuleLib.log(tn.getSymbol().getLine() + " = " + parser.getTokenNames()[tn.getSymbol().getName()] + " > " + tn.getText());
//			//ModuleLib.log(tn.getSymbol().getLine() + " = " + parser.getVocabulary().getSymbolicName(tn.getSymbol().getName()) + " > " + tn.getText() + " , " + tn.getSymbol().getStartIndex() + " > " + tn.getSymbol().getStopIndex());
//		}
		} catch (Exception ex) {

		}
	}

	@Override
	public void visitErrorNode(ErrorNode en) {
	}

}
