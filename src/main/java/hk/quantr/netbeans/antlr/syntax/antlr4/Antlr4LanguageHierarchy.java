package hk.quantr.netbeans.antlr.syntax.antlr4;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.antlr.parser.ANTLRv4Lexer;
import org.netbeans.spi.lexer.LanguageHierarchy;
import org.netbeans.spi.lexer.Lexer;
import org.netbeans.spi.lexer.LexerRestartInfo;

public class Antlr4LanguageHierarchy extends LanguageHierarchy<Antlr4TokenId> {

	private final static List<Antlr4TokenId> tokens = new ArrayList<>();
	private final static Map<Integer, Antlr4TokenId> idToTokens = new HashMap<>();

	static {
		int index = 0;
		for (int x = 0; x <= ANTLRv4Lexer.VOCABULARY.getMaxTokenType(); x++) {
			String name = ANTLRv4Lexer.VOCABULARY.getSymbolicName(x);
			if (name == null) {
				name = "INVALID" + x;
			}
			Antlr4TokenId token = new Antlr4TokenId(name, name, index);
			tokens.add(token);
			idToTokens.put(index, token);
			index++;
		}

		Antlr4TokenId tokenSelfDefine = new Antlr4TokenId("ACTION_CONTENT_FUCK", "ACTION_CONTENT_FUCK", index);
		tokens.add(tokenSelfDefine);
		idToTokens.put(index, tokenSelfDefine);
		index++;

		for (int x = 0; x <= ANTLRv4Lexer.VOCABULARY.getMaxTokenType(); x++) {
			String name = ANTLRv4Lexer.VOCABULARY.getSymbolicName(x);
			if (name == null) {
				name = "INVALID" + x;
			}

			Antlr4TokenId token = new Antlr4TokenId("DARK_" + name, "DARK_" + name, index);
			tokens.add(token);
			idToTokens.put(index, token);
			index++;
		}
	}

	public static synchronized Antlr4TokenId getToken(int id) {
		return idToTokens.get(id);
	}

	public static synchronized Antlr4TokenId getToken(String name) {
		for (Map.Entry<Integer, Antlr4TokenId> entry : idToTokens.entrySet()) {
			if (entry.getValue().name().equals(name)) {
				return entry.getValue();
			}
		}
		return null;
	}

	@Override
	protected synchronized Collection<Antlr4TokenId> createTokenIds() {
		return tokens;
	}

	@Override
	protected synchronized Lexer<Antlr4TokenId> createLexer(LexerRestartInfo<Antlr4TokenId> info) {
		return new Antlr4EditorLexer(info);
	}

	@Override
	protected String mimeType() {
		return "text/x-g4";
	}
}
