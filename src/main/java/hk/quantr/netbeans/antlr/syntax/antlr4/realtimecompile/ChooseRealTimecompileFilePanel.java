package hk.quantr.netbeans.antlr.syntax.antlr4.realtimecompile;

import hk.quantr.netbeans.antlr.syntax.antlr4.errorhighlight.ErrorHighlightingTask;
import java.awt.Dimension;
import java.io.File;
import java.util.HashMap;
import javax.swing.JOptionPane;
import org.openide.loaders.DataObject;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class ChooseRealTimecompileFilePanel extends javax.swing.JPanel {

	public static HashMap<DataObject, File> maps = new HashMap<>();

	RealTimeComboModel realTimeComboModel = new RealTimeComboModel();
	public String compileErrorMessage;

	public ChooseRealTimecompileFilePanel() {
		initComponents();
		setMaximumSize(new Dimension(150, 25));
		setPreferredSize(new Dimension(150, 25));
	}

	@SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        compileStatusLabel = new javax.swing.JLabel();
        runButton = new javax.swing.JButton();

        setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        setLayout(new java.awt.BorderLayout());

        compileStatusLabel.setForeground(java.awt.Color.white);
        compileStatusLabel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                compileStatusLabelMouseClicked(evt);
            }
        });
        add(compileStatusLabel, java.awt.BorderLayout.CENTER);

        org.openide.awt.Mnemonics.setLocalizedText(runButton, org.openide.util.NbBundle.getMessage(ChooseRealTimecompileFilePanel.class, "ChooseRealTimecompileFilePanel.runButton.text")); // NOI18N
        runButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                runButtonActionPerformed(evt);
            }
        });
        add(runButton, java.awt.BorderLayout.WEST);
    }// </editor-fold>//GEN-END:initComponents

    private void compileStatusLabelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_compileStatusLabelMouseClicked
		JOptionPane.showMessageDialog(null, compileErrorMessage);
    }//GEN-LAST:event_compileStatusLabelMouseClicked

    private void runButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_runButtonActionPerformed
		new ErrorHighlightingTask().realTimeCompile(this);
    }//GEN-LAST:event_runButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JLabel compileStatusLabel;
    private javax.swing.JButton runButton;
    // End of variables declaration//GEN-END:variables

}
