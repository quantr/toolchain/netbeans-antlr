package hk.quantr.netbeans.antlr.syntax.antlr4.errorhighlight;

import hk.quantr.netbeans.antlr.AntlrLib;
import hk.quantr.netbeans.antlr.syntax.antlr4.realtimecompile.RealTimeCompileErrorListener;
import hk.quantr.netbeans.antlr.ModuleLib;
import hk.quantr.netbeans.antlr.Antlr4TokenTableModel;
import hk.quantr.netbeans.antlr.TreeTopComponent;
import hk.quantr.netbeans.antlr.syntax.antlr4.realtimecompile.ChooseRealTimecompileFilePanel;
import hk.quantr.netbeans.antlr.syntax.antlr4.Antlr4GeneralParserListener;
import hk.quantr.netbeans.antlr.syntax.antlr4.realtimecompile.CaretListenerForTargetTable;
import hk.quantr.netbeans.antlr.syntax.antlr4.realtimecompile.RealTimeCompileHighlighter;
import hk.quantr.javalib.CommonLib;
import hk.quantr.netbeans.antlr.FileTypeG4VisualElement;
import java.awt.Color;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import javax.swing.event.CaretListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.antlr.v4.Tool;
import org.antlr.v4.parse.ANTLRParser;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.LexerInterpreter;
import org.antlr.v4.runtime.ParserInterpreter;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.RuntimeMetaData;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.antlr.v4.tool.Grammar;
import org.antlr.v4.tool.LexerGrammar;
import org.antlr.v4.tool.Rule;
import org.netbeans.api.editor.EditorRegistry;
import org.antlr.v4.tool.ast.GrammarRootAST;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.netbeans.modules.editor.NbEditorUtilities;
import org.netbeans.modules.parsing.spi.Parser.Result;
import org.netbeans.modules.parsing.spi.ParserResultTask;
import org.netbeans.modules.parsing.spi.Scheduler;
import org.netbeans.modules.parsing.spi.SchedulerEvent;
import org.netbeans.spi.editor.hints.ErrorDescription;
import org.netbeans.spi.editor.hints.ErrorDescriptionFactory;
import org.netbeans.spi.editor.hints.HintsController;
import org.netbeans.spi.editor.hints.Severity;
import org.openide.cookies.EditorCookie;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.util.Exceptions;
import org.openide.util.NbPreferences;
import org.openide.windows.TopComponent;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class ErrorHighlightingTask extends ParserResultTask {

	public static ArrayList<ErrorInfo> errorInfos = new ArrayList<>();
	public static ArrayList<ErrorInfo> targetErrorInfos = new ArrayList<>();

	@Override
	public void run(Result result, SchedulerEvent event) {
		ModuleLib.log("ErrorHighlightingTask");
		try {
			Document document = result.getSnapshot().getSource().getDocument(false);
			ArrayList<ErrorDescription> errors = new ArrayList<>();
			for (ErrorInfo errorInfo : errorInfos) {
				ErrorDescription errorDescription = ErrorDescriptionFactory.createErrorDescription(
						Severity.ERROR,
						errorInfo.message,
						document,
						document.createPosition(errorInfo.offsetStart),
						document.createPosition(errorInfo.offsetEnd + 1)
				);
				ModuleLib.log("errorDescription=" + errorDescription);
				errors.add(errorDescription);
			}

//			ErrorDescription errorDescription = ErrorDescriptionFactory.createErrorDescription(
//					Severity.ERROR,
//					"fuck you",
//					document,
//					document.createPosition(4),
//					document.createPosition(5)
//			);
//
//			errorDescription = ErrorDescriptionFactory.createErrorDescription(
//					Severity.ERROR,
//					"fuck you 2",
//					document,
//					document.createPosition(40),
//					document.createPosition(41)
//			);
			ModuleLib.log("HintsController.setErrors");
			HintsController.setErrors(document, "g4", errors);
			realTimeCompile(null);
		} catch (Exception ex) {
			ModuleLib.log("ErrorHighlightingTask exception, " + ex.getMessage());
			ex.printStackTrace();
		}
	}

	@Override
	public int getPriority() {
		return 100;
	}

	@Override
	public Class getSchedulerClass() {
		return Scheduler.EDITOR_SENSITIVE_TASK_SCHEDULER;
	}

	@Override
	public void cancel() {
	}

	public void realTimeCompile(ChooseRealTimecompileFilePanel chooseRealTimecompileFilePanel) {
		ModuleLib.log("realTimeCompile, antlr version=" + RuntimeMetaData.VERSION);
		clearTokenTable();
		TopComponent topComponent = TopComponent.getRegistry().getActivated();
		ModuleLib.log("chooseRealTimecompileFilePanel=" + chooseRealTimecompileFilePanel);
		if (chooseRealTimecompileFilePanel == null) {
			chooseRealTimecompileFilePanel = (ChooseRealTimecompileFilePanel) ModuleLib.getJComponent(topComponent, ChooseRealTimecompileFilePanel.class, "\t");
		}
		if (TreeTopComponent.compileEditorPane == null) {
			chooseRealTimecompileFilePanel.setBackground(Color.decode("#dbbf00"));
			chooseRealTimecompileFilePanel.setOpaque(true);
			clearLabel(chooseRealTimecompileFilePanel);
			chooseRealTimecompileFilePanel.compileStatusLabel.setText("Antlr windows is not open");
			chooseRealTimecompileFilePanel.repaint();
			return;
		}

		ModuleLib.log("realTimeCompile 1");
		try {
			if (chooseRealTimecompileFilePanel != null) {
				ModuleLib.log("set running");
				chooseRealTimecompileFilePanel.setBackground(Color.decode("#dbbf00"));
				chooseRealTimecompileFilePanel.setOpaque(true);
				chooseRealTimecompileFilePanel.compileStatusLabel.setText("Running");
				chooseRealTimecompileFilePanel.repaint();
			}

			ModuleLib.log("realTimeCompile 2");
			JTextComponent currentTextComponent = EditorRegistry.lastFocusedComponent();
			if (currentTextComponent == null) {
				clearLabel(chooseRealTimecompileFilePanel);
				ModuleLib.log("realTimeCompile 4, currentTextComponent=" + currentTextComponent);
				return;
			}
			ModuleLib.log("realTimeCompile 5");
			Document doc = currentTextComponent.getDocument();
			DataObject dataObject = NbEditorUtilities.getDataObject(doc);
			File currentFile = new File(dataObject.getPrimaryFile().getPath());
			if (currentFile == null) {
				ModuleLib.log("realTimeCompile 6, currentFile=" + currentFile);
				clearLabel(chooseRealTimecompileFilePanel);
				return;
			}
			File targetFile = AntlrLib.getTargetFile(dataObject);
			if (targetFile == null) {
				ModuleLib.log("realTimeCompile 7, targetFile=" + targetFile);
				clearLabel(chooseRealTimecompileFilePanel);
				return;
			}

			String targetFilePath = NbPreferences.forModule(FileTypeG4VisualElement.class).get("file-" + currentFile.getAbsolutePath(), null);
			FileObject fileObject = FileUtil.toFileObject(new File(targetFilePath));
			DataObject targetDataObject = DataObject.find(fileObject);
			EditorCookie ecA = targetDataObject.getLookup().lookup(EditorCookie.class);
			Document targetDoc = ecA.getDocument();
			if (targetDoc == null) {
				ModuleLib.log("targetDoc is null");
				clearLabel(chooseRealTimecompileFilePanel);
				return;
			}
			JTextComponent targetTextComponent = EditorRegistry.findComponent(targetDoc);
			if (targetTextComponent != null) {
				ModuleLib.log("realTimeCompile 8");
				for (CaretListener caretListener : targetTextComponent.getCaretListeners()) {
					if (caretListener instanceof CaretListenerForTargetTable) {
						targetTextComponent.removeCaretListener(caretListener);
					}
				}
				targetTextComponent.addCaretListener(new CaretListenerForTargetTable());
			}

			Tool tool = new Tool();
			GrammarRootAST ast = tool.parseGrammarFromString(currentTextComponent.getText());
			targetErrorInfos.clear();
			RealTimeCompileErrorListener targetErrorListener = new RealTimeCompileErrorListener(targetDataObject, targetErrorInfos);
			refreshTokenTable(currentTextComponent.getText(), IOUtils.toString(new FileInputStream(targetFile), "utf-8"));
			if (ast.grammarType == ANTLRParser.COMBINED) {
				ModuleLib.log("realTimeCompile ANTLRParser.COMBINED, ast.grammarType=" + ast.grammarType);
//				Grammar grammar = tool.createGrammar(ast);
//				tool.process(grammar, false);
				Grammar grammar = new Grammar(currentTextComponent.getText());
				ModuleLib.log("targetFile=" + targetFile);
				LexerInterpreter lexer = grammar.createLexerInterpreter(new ANTLRInputStream(new FileInputStream(targetFile)));
				lexer.removeErrorListeners();
				lexer.addErrorListener(targetErrorListener);

				CommonTokenStream tokenStream;
				if (NbPreferences.forModule(FileTypeG4VisualElement.class).get("commonTokenStreamIndex", null) == null) {
					tokenStream = new CommonTokenStream(lexer);
				} else {
					tokenStream = new CommonTokenStream(lexer, Integer.parseInt(NbPreferences.forModule(FileTypeG4VisualElement.class).get("commonTokenStreamIndex", null)));
				}
				ParserInterpreter parser = grammar.createParserInterpreter(tokenStream);
//				parser.getInterpreter().setPredictionMode(PredictionMode.LL);
				parser.removeErrorListeners();
				parser.addErrorListener(targetErrorListener);
//				parser.setErrorHandler(new DefaultErrorStrategy() {
//
//					@Override
//					public void recover(org.antlr.v4.runtime.Parser recognizer, RecognitionException e) {
//						for (ParserRuleContext context = recognizer.getContext(); context != null; context = context.getParent()) {
//							context.exception = e;
//						}
//
////						throw new ParseCancellationException(e);
//					}
//
//					@Override
//					public Token recoverInline(org.antlr.v4.runtime.Parser recognizer)
//							throws RecognitionException {
//						InputMismatchException e = new InputMismatchException(recognizer);
//						for (ParserRuleContext context = recognizer.getContext(); context != null; context = context.getParent()) {
//							context.exception = e;
//						}
//
////						throw new ParseCancellationException(e);
//					}
//				});

				String startRule = AntlrLib.getStartRule(dataObject);
				if (startRule == null) {
					ModuleLib.log("start rule is null");
					clearLabel(chooseRealTimecompileFilePanel);
					return;
				}

				Rule start = grammar.getRule(startRule);
				ParserRuleContext parserRuleContext = parser.parse(start.index);
				ParseTreeWalker walker = new ParseTreeWalker();
				Antlr4GeneralParserListener listener = new Antlr4GeneralParserListener(parser, lexer);
				walker.walk(listener, parserRuleContext);

				for (Token t : tokenStream.getTokens()) {
					ModuleLib.log("token=" + t.getText());
				}

//				List<Token> tokens = tokenStream.getTokens();
//				for (Token token : tokens) {
//					if (token.getType() != Token.EOF) {
//						System.out.println("lexer1=" + lexer.getRuleNames()[token.getType()]);
//						System.out.println("lexer2=" + lexer.getTokenNames()[token.getType()]);
////						System.out.println("lexer3=" + lexer.getModeNames()[token.getType()]);
//						System.out.println("lexer4=" + parser.getVocabulary().getLiteralName(token.getType()));
//						System.out.println("lexer4=" + parser.getVocabulary().getDisplayName(token.getType()));
//						System.out.println("lexer4=" + parser.getVocabulary().getSymbolicName(token.getType()));
//					}
//				}
			} else {
				ModuleLib.log("realTimeCompile not ANTLRParser.COMBINED, ast.grammarType=" + ast.grammarType);
				File lexerGrammarFile = AntlrLib.getLexerGrammarFile(dataObject);
				ModuleLib.log("lexerGrammarFile=" + lexerGrammarFile);
				if (lexerGrammarFile == null || !lexerGrammarFile.isFile() || !lexerGrammarFile.exists()) {
					ModuleLib.log("realTimeCompile lexerGrammarFile is null");
					chooseRealTimecompileFilePanel.setBackground(new Color(131, 16, 16));
					chooseRealTimecompileFilePanel.setOpaque(true);
					chooseRealTimecompileFilePanel.compileStatusLabel.setText("No lexer file");
					chooseRealTimecompileFilePanel.compileErrorMessage = "Please set the lexer file";
					chooseRealTimecompileFilePanel.repaint();
				} else {
					String strLexer = IOUtils.toString(new FileInputStream(lexerGrammarFile), "utf-8");
					String strParser = currentTextComponent.getText();
					LexerGrammar lg = new LexerGrammar(strLexer);
					if (lg == null) {
						ModuleLib.log("lg is null");
						return;
					}
					Grammar grammar = new Grammar(strParser, lg);

					//ANTLRFileStream input = new ANTLRFileStream("/Users/peter/workspace/pfsbuilder/src/main/java/com/pfsbuilder/MainFrame2.java");
					//LexerInterpreter lexerInterpreter = lg.createLexerInterpreter(input);
					LexerInterpreter lexerInterpreter = lg.createLexerInterpreter(new ANTLRInputStream(new FileInputStream(targetFile)));

					lexerInterpreter.removeErrorListeners();
					lexerInterpreter.addErrorListener(targetErrorListener);

					CommonTokenStream tokenStream;
					if (NbPreferences.forModule(FileTypeG4VisualElement.class).get("commonTokenStreamIndex", null) == null) {
						tokenStream = new CommonTokenStream(lexerInterpreter);
					} else {
						tokenStream = new CommonTokenStream(lexerInterpreter, Integer.parseInt(NbPreferences.forModule(FileTypeG4VisualElement.class).get("commonTokenStreamIndex", null)));
					}
					System.out.println(grammar.getATN());
					ParserInterpreter parser = grammar.createParserInterpreter(tokenStream);
					parser.removeErrorListeners();
					parser.addErrorListener(targetErrorListener);

					String startRule = AntlrLib.getStartRule(dataObject);
					if (startRule == null) {
						ModuleLib.log("start rule is null");
						clearLabel(chooseRealTimecompileFilePanel);
						return;
					}
					Rule start = grammar.getRule(startRule);
					ParserRuleContext parserRuleContext = parser.parse(start.index);
					ParseTreeWalker walker = new ParseTreeWalker();
					Antlr4GeneralParserListener listener = new Antlr4GeneralParserListener(parser, lexerInterpreter);
					walker.walk(listener, parserRuleContext);
				}
			}

			RealTimeCompileHighlighter realTimeCompileHighlighter = (RealTimeCompileHighlighter) targetDoc.getProperty(RealTimeCompileHighlighter.class);
			if (realTimeCompileHighlighter != null) {
				realTimeCompileHighlighter.bag.clear();
				String tempText = targetTextComponent.getText();
				for (ErrorInfo errorInfo : targetErrorInfos) {
//					int countStart = StringUtils.countMatches(tempText.substring(0, errorInfo.offsetStart), "\n");
//					int countEnd = StringUtils.countMatches(tempText.substring(0, errorInfo.offsetEnd > tempText.length() ? tempText.length() : errorInfo.offsetEnd), "\n");
//					realTimeCompileHighlighter.bag.addHighlight(errorInfo.offsetStart - countStart + 1, errorInfo.offsetEnd - countEnd + 1, RealTimeCompileHighlighter.defaultColors);
					System.out.println("errorInfo.offsetStart=" + errorInfo.offsetStart + " , " + errorInfo.offsetEnd);
					int offsetEnd = errorInfo.offsetEnd > tempText.length() ? tempText.length() : errorInfo.offsetEnd;
					realTimeCompileHighlighter.bag.addHighlight(errorInfo.offsetStart, offsetEnd, RealTimeCompileHighlighter.defaultColors);
				}

//				OffsetsBag bag = new OffsetsBag(doc);
//				bag.addHighlight(5, 10, AttributesUtilities.createImmutable(StyleConstants.Background, Color.cyan));
//				HighlightsContainer[] layers = new HighlightsContainer[]{bag};
//				ProxyHighlightsContainer hb = new ProxyHighlightsContainer(doc, layers);
//				hb.setLayers(doc, layers);
				if (chooseRealTimecompileFilePanel != null) {
					if (targetErrorListener.compileError) {
						chooseRealTimecompileFilePanel.setBackground(new Color(131, 16, 16));
						chooseRealTimecompileFilePanel.setOpaque(true);
						chooseRealTimecompileFilePanel.compileStatusLabel.setText("Failed");
						chooseRealTimecompileFilePanel.compileErrorMessage = targetErrorListener.compileErrorMessage;
						chooseRealTimecompileFilePanel.repaint();
						TreeTopComponent.compileEditorPane.setText(targetErrorListener.compileErrorMessage);
					} else {
						chooseRealTimecompileFilePanel.setBackground(new Color(16, 131, 16));
						chooseRealTimecompileFilePanel.setOpaque(true);
						chooseRealTimecompileFilePanel.compileStatusLabel.setText("Success");
						chooseRealTimecompileFilePanel.compileErrorMessage = null;
						chooseRealTimecompileFilePanel.repaint();
						TreeTopComponent.compileEditorPane.setText("No error");
					}
				} else {
					clearLabel(chooseRealTimecompileFilePanel);
				}
				updateTargetTable(targetDoc);
			}
		} catch (Exception ex) {
			if (chooseRealTimecompileFilePanel != null) {
				chooseRealTimecompileFilePanel.setBackground(new Color(131, 16, 16));
				chooseRealTimecompileFilePanel.setOpaque(true);
				chooseRealTimecompileFilePanel.compileStatusLabel.setText("Failed");
				chooseRealTimecompileFilePanel.compileErrorMessage = ex.getMessage();
			}
			Exceptions.printStackTrace(ex);
		}
		ModuleLib.log("end");
	}

	void clearLabel(ChooseRealTimecompileFilePanel chooseRealTimecompileFilePanel) {
		if (chooseRealTimecompileFilePanel != null) {
			chooseRealTimecompileFilePanel.setOpaque(false);
			chooseRealTimecompileFilePanel.compileStatusLabel.setText(null);
			chooseRealTimecompileFilePanel.repaint();
		}
	}

	private void clearTokenTable() {
		if (TreeTopComponent.treeTopComponent == null) {
			return;
		}
		Antlr4TokenTableModel targetTableModel = TreeTopComponent.treeTopComponent.tokenTableModel;
		if (targetTableModel == null) {
			return;
		}
		targetTableModel.lines.clear();
		targetTableModel.fireTableStructureChanged();
	}

	private void updateTargetTable(Document targetDoc) {
		try {
			String str = targetDoc.getText(0, targetDoc.getLength());
			if (str == null) {
				return;
			}
			String lines[] = str.split("\n");
			if (TreeTopComponent.treeTopComponent == null) {
				return;
			}
			Antlr4TokenTableModel targetTableModel = TreeTopComponent.treeTopComponent.tokenTableModel;
			if (targetTableModel == null) {
				return;
			}
			targetTableModel.lines.clear();
			targetTableModel.lines.addAll(Arrays.asList(lines));
			targetTableModel.fireTableStructureChanged();
			CommonLib.autoResizeColumn(TreeTopComponent.treeTopComponent.tokenTable);

			TreeTopComponent.treeTopComponent.refreshTargetJgraphButtonActionPerformed();
		} catch (BadLocationException ex) {
			Exceptions.printStackTrace(ex);
		}
	}

	private void refreshTokenTable(String grammarFileContent, String targetFileContent) {
		if (TreeTopComponent.treeTopComponent != null) {
			Antlr4TokenTableModel tokenTableModel = TreeTopComponent.treeTopComponent.tokenTableModel;
			if (tokenTableModel != null) {
				tokenTableModel.tokens.clear();

				Tool tool = new Tool();
				GrammarRootAST ast = tool.parseGrammarFromString(grammarFileContent);
				ArrayList<Token> tokens = null;
				if (ast.grammarType == ANTLRParser.COMBINED) {
					try {
						Grammar grammar = new Grammar(grammarFileContent);
						LexerInterpreter lexer = grammar.createLexerInterpreter(new ANTLRInputStream(targetFileContent));
						tokenTableModel.vocabulary = lexer.getVocabulary();
						int lastLine = -1;
						for (Token token : lexer.getAllTokens()) {
							int lineNo = token.getLine();
							if (lineNo != lastLine) {
								tokens = new ArrayList<Token>();
								lastLine = lineNo;
							}
							String tokenName = lexer.getVocabulary().getSymbolicName(token.getType());
//							ModuleLib.log("refreshTokenTable ---------------- " + tokenName + " : " + lineNo + " = " + token.getStartIndex() + " > " + token.getStopIndex());
							tokens.add(token);

							tokenTableModel.tokens.put(lineNo, tokens);
						}
					} catch (Exception ex) {
						Exceptions.printStackTrace(ex);
					}
				} else {
					try {
						JTextComponent currentTextComponent = EditorRegistry.lastFocusedComponent();
						if (currentTextComponent == null) {
							ModuleLib.log("currentTextComponent is null");
							return;
						}
						Document doc = currentTextComponent.getDocument();
						DataObject dataObject = NbEditorUtilities.getDataObject(doc);
						File lexerGrammarFile = AntlrLib.getLexerGrammarFile(dataObject);
						if (lexerGrammarFile == null || !lexerGrammarFile.isFile() || !lexerGrammarFile.exists()) {
							ModuleLib.log("realTimeCompile lexerGrammarFile is null");
						} else {
							String strLexer = IOUtils.toString(new FileInputStream(lexerGrammarFile), "utf-8");
							String strParser = grammarFileContent;
							LexerGrammar lg = new LexerGrammar(strLexer);
							Grammar grammar = new Grammar(strParser, lg);

//							ModuleLib.log("refreshTokenTable =" + targetFileContent.substring(0, 15));
							LexerInterpreter lexerInterpreter = lg.createLexerInterpreter(new ANTLRInputStream(targetFileContent));

							tokenTableModel.vocabulary = lexerInterpreter.getVocabulary();
							int lastLine = -1;
							for (Token token : lexerInterpreter.getAllTokens()) {
								int lineNo = token.getLine();
								if (lineNo != lastLine) {
									tokens = new ArrayList<Token>();
									lastLine = lineNo;
								}
								String tokenName = lexerInterpreter.getVocabulary().getSymbolicName(token.getType());
//								ModuleLib.log("refreshTokenTable ---------------- " + tokenName + " : " + lineNo + " = " + token.getStartIndex() + " > " + token.getStopIndex());
								tokens.add(token);

								tokenTableModel.tokens.put(lineNo, tokens);
							}
						}
					} catch (Exception ex) {
						Exceptions.printStackTrace(ex);
					}
				}
			}
		}
	}

}
