// License : Apache License Version 2.0  https://www.apache.org/licenses/LICENSE-2.0
package hk.quantr.netbeans.antlr.verilogaction;

import hk.quantr.netbeans.antlr.syntax.antlr4.errorhighlight.ErrorHighlightingTask;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import org.openide.loaders.DataObject;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.NbBundle.Messages;

@ActionID(
		category = "Build",
		id = "hk.quantr.netbeans.antlr.verilogaction.RunAnalysisAction"
)
@ActionRegistration(
		displayName = "#CTL_RunAnalysisAction",
		iconInMenu = true,
		iconBase = "hk/quantr/netbeans/antlr/antlr.png"
)
@ActionReferences({
	@ActionReference(path = "Loaders/text/x-g4/Actions", position = 1000),
	@ActionReference(path = "Editors/TabActions", position = 1000)
})
@Messages("CTL_RunAnalysisAction=Run Analysis")
public final class RunAnalysisAction implements ActionListener {

	private final DataObject context;

	public RunAnalysisAction(DataObject context) {
		this.context = context;
	}

	@Override
	public void actionPerformed(ActionEvent ev) {
		new ErrorHighlightingTask().realTimeCompile(null);
	}
}
