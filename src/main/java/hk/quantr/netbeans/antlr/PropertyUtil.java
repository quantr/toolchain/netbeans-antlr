package hk.quantr.netbeans.antlr;

import java.util.Properties;

public class PropertyUtil {
	static Properties prop = null;

	public static String getProperty(String name) {
		if (prop == null) {
			prop = new Properties();
			try {
				prop.load(PropertyUtil.class.getClassLoader().getResourceAsStream("/hk/quantr/netbeans/antlr/netbeansAntlr.properties"));
			} catch (Exception e) {
//				e.printStackTrace();
			}
		}

		return prop.getProperty(name);
	}
}
