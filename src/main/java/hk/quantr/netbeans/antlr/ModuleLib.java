package hk.quantr.netbeans.antlr;

import java.awt.Color;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JComponent;
import javax.swing.UIManager;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.EditorRegistry;
import org.netbeans.api.io.IOProvider;
import org.netbeans.api.io.InputOutput;

public class ModuleLib {

	public static boolean isDebug = false;
	static SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:s.S");
	static String themeName = UIManager.getLookAndFeel().getName();

	public static void log(String str) {
		if (isDebug) {
			InputOutput io = IOProvider.getDefault().getIO("Antlr", false);
			io.getOut().println(sdf.format(new Date()) + " - " + str);
		} else {
			System.err.println(str);
		}
	}

	public static void log(Object obj) {
		if (obj != null) {
			log(obj.toString());
		}
	}

	public static void logNoNewLine(String str) {
		if (isDebug) {
			InputOutput io = IOProvider.getDefault().getIO("Antlr", false);
			io.getOut().print(sdf.format(new Date()) + " - " + str);
		} else {
			System.err.println(str);
		}
	}

	public static void logNoNewLine(Object obj) {
		log(obj.toString());
	}

	public static String printException(Exception ex) {
		StringWriter errors = new StringWriter();
		ex.printStackTrace(new PrintWriter(errors));
		return errors.toString();
	}

	public static boolean isMac() {
		if (System.getProperty("os.name").toLowerCase().contains("mac")) {
			return true;
		} else {
			return false;
		}
	}

	public static void print(JComponent component, String str) {
		ModuleLib.log("+++ " + str + component);
		for (int x = 0; x < component.getComponentCount(); x++) {
			if (component.getComponent(x) instanceof JComponent) {
				print((JComponent) component.getComponent(x), str + "\t");
			}
		}
	}

	public static JComponent getJComponent(JComponent component, Class c, String str) {
		//ModuleLib.log("---- " + str + component.getClass() + " == " + c + " > " + (component.getClass() == c));
		if (component.getClass() == c) {
			return component;
		}
		for (int x = 0; x < component.getComponentCount(); x++) {
			if (component.getComponent(x) instanceof JComponent) {
				JComponent temp = getJComponent((JComponent) component.getComponent(x), c, str + "\t");
				if (temp != null) {
					return temp;
				}
			}
		}
		return null;
	}

	public static boolean isDarkTheme() {
		JTextComponent jTextComponent = EditorRegistry.lastFocusedComponent();
		if (jTextComponent == null) {
			return false;
		} else {
			if (isColorDark(jTextComponent.getBackground())) {
				return true;
			} else {
				return false;
			}
		}
//		return true;
	}

	public static boolean isColorDark(Color color) {
		float[] hsb = Color.RGBtoHSB(color.getRed(), color.getGreen(), color.getBlue(), null);
		float brightness = hsb[2];
		return brightness < 0.5;
	}
}
