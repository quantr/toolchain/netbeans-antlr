// License : Apache License Version 2.0  https://www.apache.org/licenses/LICENSE-2.0
package hk.quantr.netbeans.antlr;

import java.util.ArrayList;
import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;

/**
 *
 * @author peter
 */
public class ErrorListenerForDot extends BaseErrorListener {

	class ErrorInfo {

		Recognizer<?, ?> recognizer;
		Object offendingSymbol;
		int line;
		int charPositionInLine;
		String msg;
		RecognitionException e;

		public ErrorInfo(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine, String msg, RecognitionException e) {
			this.recognizer = recognizer;
			this.offendingSymbol = offendingSymbol;
			this.line = line;
			this.charPositionInLine = charPositionInLine;
			this.msg = msg;
			this.e = e;
		}
	}

	public ArrayList<ErrorInfo> errorInfos = new ArrayList<>();

	public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine, String msg, RecognitionException e) {
		errorInfos.add(new ErrorInfo(recognizer, offendingSymbol, line, charPositionInLine, msg, e));
	}
}
