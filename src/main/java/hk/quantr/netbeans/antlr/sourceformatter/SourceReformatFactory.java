// License : Apache License Version 2.0  https://www.apache.org/licenses/LICENSE-2.0
package hk.quantr.netbeans.antlr.sourceformatter;

import com.khubla.antlr4formatter.Antlr4Formatter;
import com.khubla.antlr4formatter.Antlr4FormatterException;
import javax.swing.text.BadLocationException;
import org.netbeans.modules.editor.indent.spi.Context;
import org.netbeans.modules.editor.indent.spi.ExtraLock;
import org.netbeans.modules.editor.indent.spi.ReformatTask;
import org.openide.util.Exceptions;

/**
 *
 * @author Peter
 */
public class SourceReformatFactory implements ReformatTask.Factory {

	@Override
	public ReformatTask createTask(Context cntxt) {
		return new SourceReformat(cntxt);
	}

	public class SourceReformat implements ReformatTask {

		Context ctx;

		public SourceReformat(Context c) {
			this.ctx = c;
		}

		@Override
		public void reformat() throws BadLocationException {
			try {
				String toFormat = ctx.document().getText(ctx.startOffset(), ctx.endOffset());

				ctx.document().remove(ctx.startOffset(), ctx.endOffset() - ctx.startOffset());
				ctx.document().insertString(ctx.startOffset(), Antlr4Formatter.format(toFormat), null);
			} catch (Antlr4FormatterException ex) {
				Exceptions.printStackTrace(ex);
			}
		}

		@Override
		public ExtraLock reformatLock() {
			return null;
		}
	}
}
