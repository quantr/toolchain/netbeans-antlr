lexer grammar RISCVAssemblerLexer;

WS		:	(' '|'\t')+ -> skip;
NL		:	'\r'? '\n' -> skip;
LINE_COMMENT	:	';' ~[\r\n]*;

ADD_		:	'+';
MIN_		:	'-';
MUL_		:	'*';
DIV_		:	'/';
MOD_		:	'%';
SQU_		:	'^';

MATH_EXPRESSION	:	('0x' [0-9a-zA-Z]+ | [0-9]+) (
						'0x' [0-9a-zA-Z]+
						|	[0-9]+
						|	ADD_
						|	MIN_
						|	MUL_
						|	DIV_
						|	WS
						|	OPEN_SMALL_BRACKET MATH_EXPRESSION CLOSE_SMALL_BRACKET
					)*
				;

COMMA		:	',';
COLON		:	':';
DB_SYMBOL	:	'db';

OPEN_BIG_BRACKET		:	'{';
CLOSE_BIG_BRACKET	:	'}';

OPEN_SMALL_BRACKET	:	'(';
CLOSE_SMALL_BRACKET	:	')';

OPEN_MID_BRACKET		:	'[';
CLOSE_MID_BRACKET	:	']';

X0		:	'x0';
X1		:	'x1';
X2		:	'x2';
X3		:	'x3';
X4		:	'x4';
X5		:	'x5';
X6		:	'x6';
X7		:	'x7';
X8		:	'x8';
X9		:	'x9';
X10		:	'x10';
X11		:	'x11';
X12		:	'x12';
X13		:	'x13';
X14		:	'x14';
X15		:	'x15';
X16		:	'x16';
X17		:	'x17';
X18		:	'x18';
X19		:	'x19';
X20		:	'x20';
X21		:	'x21';
X22		:	'x22';
X23		:	'x23';
X24		:	'x24';
X25		:	'x25';
X26		:	'x26';
X27		:	'x27';
X28		:	'x28';
X29		:	'x29';
X30		:	'x30';
X31		:	'x31';
ZERO		:	'zero';
RA		:	'ra';
SP		:	'sp';
GP		:	'gp';
TP		:	'tp';
T0		:	't0';
T1		:	't1';
T2		:	't2';
S0		:	's0';
FP		:	'fp';
S1		:	's1';
A0		:	'a0';
A1		:	'a1';
A2		:	'a2';
A3		:	'a3';
A4		:	'a4';
A5		:	'a5';
A6		:	'a6';
A7		:	'a7';
S2		:	's2';
S3		:	's3';
S4		:	's4';
S5		:	's5';
S6		:	's6';
S7		:	's7';
S8		:	's8';
S9		:	's9';
S10		:	's10';
S11		:	's11';
T3		:	't3';
T4		:	't4';
T5		:	't5';
T6		:	't6';

SLL     :   'sll';
SLLI    :   'slli';
SRL     :   'srl';
SRLI    :   'srli';
SRA     :   'sra';
SRAI    :   'srai';
ADD     :   'add';
ADDI    :   'addi';
SUB     :   'sub';
LUI     :   'lui';
AUIPC   :   'auipc';
XOR     :   'xor';
XORI    :   'xori';
OR      :   'or';
ORI     :   'ori';
AND     :   'and';
ANDI    :   'andi';
SLT     :   'slt';
SLTI    :   'slti';
SLTU    :   'sltu';
SLTIU   :   'sltiu';
BEQ     :   'beq';
BNE		:	'bne';
BLT		:	'blt';
BGR		:	'bge';
BLTU		:	'bltu';
BGEU		:	'bgeu';
JAL		:	'jal';
JALR		:	'jalr';
FENCE	:	'fence';
FENCEI	:	'fence.i';
ECALL	:	'ecall';
EBREAK	:	'ebreak';


IDENTIFIER	:	[a-zA-Z] [a-zA-Z0-9]+;

SHAMT		:	'0x'? [a-f0-9]+;

