parser grammar AssemblerParser;

options { tokenVocab=AssemblerLexer; }


@header {
	import hk.quantr.javalib.CommonLib;
	import hk.quantr.assembler.MAL;
	import hk.quantr.antlrcalculatorlibrary.CalculatorLibrary;
}

@parser::members{
	public MAL mal = new MAL();
        
}
            
r8  	returns[int operandSize,int col]:
                        AH{ mal.rex_rb = false;mal.modrm_row = 28; $operandSize=8; $col = 4;}|AL { mal.rex_rb = false;mal.modrm_row = 24; $operandSize=8; $col = 0;}
			|BH{mal.rex_rb = false; mal.modrm_row= 31; $operandSize=8; $col = 7;}|BL { mal.rex_rb = false;mal.modrm_row = 27; $operandSize=8; $col = 3;}
			|CH {mal.rex_rb = false; mal.modrm_row = 29;$operandSize=8; $col = 5;}|CL { mal.rex_rb = false;mal.modrm_row = 25;$operandSize=8; $col = 1;}
			|DH { mal.rex_rb = false;mal.modrm_row = 30;$operandSize=8; $col = 6;}|DL { mal.rex_rb = false;mal.modrm_row = 26;$operandSize=8; $col = 2;}
			|R8L{mal.rex_rb = true;$operandSize=8;mal.modrm_row = -241;$col =0;}
			|R9L{mal.rex_rb = true;$operandSize=8;mal.modrm_row = -251;$col =1;}
			|R10L{mal.rex_rb = true;$operandSize=8;mal.modrm_row = -261;$col =2;}
			|R11L{mal.rex_rb = true;$operandSize=8;mal.modrm_row = -271;$col =3;}
			|R12L{mal.rex_rb = true;$operandSize=8;mal.modrm_row = -281;$col =4;}
			|R13L{mal.rex_rb = true;$operandSize=8;mal.modrm_row = -291;$col =5;}
			|R14L{mal.rex_rb = true;$operandSize=8;mal.modrm_row = -301;$col =6;}
			|R15L{mal.rex_rb = true;$operandSize=8;mal.modrm_row = -311;$col =7;}
			|BPL {mal.rex_rb = false; $operandSize=8;mal.modrm_row = 2911;$col = 5;}
			|SPL {mal.rex_rb = false; $operandSize=8;mal.modrm_row = 2811;$col = 4;}
			|DIL {mal.rex_rb = false; $operandSize=8;mal.modrm_row = 3111;$col = 7;}
			|SIL {mal.rex_rb = false; $operandSize=8;mal.modrm_row = 3011;$col = 6;}
			;
