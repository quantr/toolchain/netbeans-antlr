parser grammar RISCVAssemblerParser;
options { tokenVocab=RISCVAssemblerLexer; }

@header {
	import hk.quantr.javalib.CommonLib;
	import hk.quantr.assembler.riscv.RISCVEncoder;
	import hk.quantr.assembler.riscv.Label;
	import hk.quantr.antlrcalculatorlibrary.CalculatorLibrary;
}

@parser::members{
	public RISCVEncoder encoder = new RISCVEncoder();
        public ArrayList<Label> labels=new ArrayList<>();
}


assemble		:	lines EOF
			;

lines		:	line*
			;

line			:	instructions
			;

comment		:	LINE_COMMENT
			;

label		:	IDENTIFIER COLON	{}
			;

instructions		:	shifts
				|	arithmetic
				|	logical
				|	branches
				|	jump
				|	synch
				|	environment
				;

shifts		:	sll
			|	slli
			|	srl
			|	srli
			|	sra
			|	srai
			;

arithmetic	:	add
			|	addi
			|	sub
			|	lui
			|	auipc
			;

logical		:	xor
			|	xori
			|	or
			|	ori
			|	and
			|	andi
			;

branches		:	beq
			|	bne
			|	blt
			|	bge
			|	bltu
			|	bgeu
			;

jump			:	jal
			|	jalr
			;

synch		:	fence
			|	fencei
			;

environment	:	ecall
			|	ebreak
			;

rs1			returns[int type]:	registers {$type=$registers.type;};
rs2			returns[int type]:	registers {$type=$registers.type;};
rd			returns[int type]:	registers {$type=$registers.type;};

registers	returns[int type]:	X0 {$type=0;}
			|	X1 {$type=1;}
			|	X2 {$type=2;}
			|	X3 {$type=3;}
			|	X4 {$type=4;}
			|	X5 {$type=5;}
			|	X6 {$type=6;}
			|	X7 {$type=7;}
			|	X8 {$type=8;}
			|	X9 {$type=9;}
			|	X10 {$type=10;}
			|	X11 {$type=10;}
			|	X12 {$type=10;}
			|	X13 {$type=10;}
			|	X14 {$type=10;}
			|	X15 {$type=10;}
			|	X16 {$type=10;}
			|	X17 {$type=10;}
			|	X18 {$type=10;}
			|	X19 {$type=10;}
			|	X20 {$type=20;}
			|	X21 {$type=21;}
			|	X22 {$type=22;}
			|	X23 {$type=23;}
			|	X24 {$type=24;}
			|	X25 {$type=25;}
			|	X26 {$type=26;}
			|	X27 {$type=27;}
			|	X28 {$type=28;}
			|	X29 {$type=29;}
			|	X30 {$type=30;}
			|	X31 {$type=31;}
			|	F0
			|	F1
			|	F2
			|	F3
			|	F4
			|	F5
			|	F6
			|	F7
			|	F8
			|	F9
			|	F10
			|	F11
			|	F12
			|	F13
			|	F14
			|	F15
			|	F16
			|	F17
			|	F18
			|	F19
			|	F20
			|	F21
			|	F22
			|	F23
			|	F24
			|	F25
			|	F26
			|	F27
			|	F28
			|	F29
			|	F30
			|	F31
			|	ZERO {$type=0;}
			|	RA {$type=1;}
			|	SP {$type=2;}
			|	GP {$type=3;}
			|	TP {$type=4;}
			|	T0 {$type=5;}
			|	T1 {$type=6;}
			|	T2 {$type=7;}
			|	S0 {$type=8;}
			|	FP {$type=8;}
			|	S1 {$type=9;}
			|	A0 {$type=10;}
			|	A1
			|	A2
			|	A3
			|	A4
			|	A5
			|	A6
			|	A7
			|	S2
			|	S3
			|	S4
			|	S5
			|	S6
			|	S7
			|	S8
			|	S9
			|	S10
			|	S11
			|	T3
			|	T4
			|	T5
			|	T6
			|	FT0
			|	FT1
			|	FT2
			|	FT3
			|	FT4
			|	FT5
			|	FT6
			|	FT7
			|	FS0
			|	FS1
			|	FA0
			|	FA1
			|	FA2
			|	FA3
			|	FA4
			|	FA5
			|	FA6
			|	FA7
			|	FS2
			|	FS3
			|	FS4
			|	FS5
			|	FS6
			|	FS7
			|	FS8
			|	FS9
			|	FS10
			|	FS11
			|	FT8
			|	FT9
			|	FT10
			|	FT11
			;

imm			:	MATH_EXPRESSION
			;

sll			:	SLL		rd COMMA rs1 COMMA rs2	{encoder.encodeTypeR(0b0110011,0b001,0b0000000,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
slli		:	SLLI	rd COMMA rs1 COMMA SHAMT	{};
srl			:	SRL		rd COMMA rs1 COMMA rs2	{encoder.encodeTypeR(0b0110011,0b101,0b0000000,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
srli		:	SRLI	rd COMMA rs1 COMMA SHAMT	{};
sra			:	SRA		rd COMMA rs1 COMMA rs2	{encoder.encodeTypeR(0b0110011,0b101,0b0100000,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
srai		:	SRAI	rd COMMA rs1 COMMA SHAMT	{};

add			:	ADD		rd COMMA rs1 COMMA rs2	{encoder.encodeTypeR(0b0110011,0b111,0b0000000,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
addi		:	ADDI	rd COMMA rs1 COMMA imm	{encoder.encodeTypeI(0b0010011, 0b000, $rd.ctx.type, $rs1.ctx.type, CalculatorLibrary.cal($imm.text));};
sub			:	SUB		rd COMMA rs1 COMMA rs2	{encoder.encodeTypeR(0b0110011,0b000,0b0000000,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
lui			:	LUI		rd COMMA imm				{encoder.encodeTypeU(0b0110111, $rd.ctx.type, CalculatorLibrary.cal($imm.text));};
auipc		:	AUIPC	rd COMMA imm				{encoder.encodeTypeU(0b0010111, $rd.ctx.type, CalculatorLibrary.cal($imm.text));};

xor			:	XOR		rd COMMA rs1 COMMA rs2	{encoder.encodeTypeR(0b0110011,0b100,0b0000000,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
xori		:	XORI	rd COMMA rs1 COMMA imm	{encoder.encodeTypeI(0b0010011, 0b100, $rd.ctx.type, $rs1.ctx.type, CalculatorLibrary.cal($imm.text));};
or			:	OR		rd COMMA rs1 COMMA rs2	{encoder.encodeTypeR(0b0110011,0b110,0b0000000,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
ori			:	ORI		rd COMMA rs1 COMMA imm	{encoder.encodeTypeI(0b0010011, 0b110, $rd.ctx.type, $rs1.ctx.type, CalculatorLibrary.cal($imm.text));};
and			:	AND		rd COMMA rs1 COMMA rs2	{encoder.encodeTypeR(0b0110011,0b111,0b0000000,$rd.ctx.type,$rs1.ctx.type,$rs2.ctx.type);};
andi		:	ANDI	rd COMMA rs1 COMMA imm	{encoder.encodeTypeI(0b0010011, 0b111, $rd.ctx.type, $rs1.ctx.type, CalculatorLibrary.cal($imm.text));};

beq			:	BEQ		rs1 COMMA rs2 COMMA imm	{encoder.encodeTypeB(0b1100011, 0b000, $rs1.ctx.type, $rs2.ctx.type, CalculatorLibrary.cal($imm.text));};
bne			:	BNE		rs1 COMMA rs2 COMMA imm	{};
blt			:	BLT		rs1 COMMA rs2 COMMA imm	{};
bge			:	BGE		rs1 COMMA rs2 COMMA imm	{};
bltu		:	BLTU	rs1 COMMA rs2 COMMA imm	{};
bgeu		:	BGEU	rs1 COMMA rs2 COMMA imm	{};

jal			:	JAL		rd COMMA imm				{encoder.encodeTypeJ(0b1101111, $rd.ctx.type, CalculatorLibrary.cal($imm.text));};
jalr		:	JALR	rd COMMA rs1 COMMA imm	{};

fence		:	FENCE							{};
fencei		:	FENCEI							{};

ecall		:	ECALL							{};
ebreak		:	EBREAK							{};
