package antlr;

import java.nio.file.Files;
import java.nio.file.Paths;
import org.antlr.v4.Tool;
import org.antlr.v4.tool.Grammar;
import org.antlr.v4.tool.ast.GrammarRootAST;
import org.junit.Test;

// License : Apache License Version 2.0  https://www.apache.org/licenses/LICENSE-2.0
/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class TestDynamicParser {

	@Test
	public void testDynamicParser() throws Exception {
//		Tool tool = new Tool();
//		String content = new String(Files.readAllBytes(Paths.get(getClass().getResource("Assembler.g4").toURI())));
//		GrammarRootAST ast = tool.parseGrammarFromString(content);
//		Grammar grammar = tool.createGrammar(ast);
//		tool.process(grammar, false);
//		for (String rule : grammar.getRuleNames()) {
//			System.out.println(rule);
//		}

		String content = new String(Files.readAllBytes(Paths.get(getClass().getResource("Assembler.g4").toURI())));
		Tool tool = new Tool();
		GrammarRootAST ast = tool.parseGrammarFromString(content);
		Grammar grammar = new Grammar(content);
		System.out.println(ast.grammarType);
		for (String rule : grammar.getRuleNames()) {
			System.out.println(rule);
		}
	}
}
