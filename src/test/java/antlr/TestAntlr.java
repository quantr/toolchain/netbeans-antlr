package antlr;

//import hk.quantr.AstNode;
import java.io.FileInputStream;
import org.antlr.parser.ANTLRv4Lexer;
import org.antlr.parser.ANTLRv4Parser;
import org.antlr.parser.ANTLRv4ParserBaseListener;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.junit.Test;

public class TestAntlr {

	@Test
	public void test() throws Exception {
		System.out.println("test()");
//		ANTLRv4Lexer lexer = new ANTLRv4Lexer(new ANTLRInputStream(getClass().getResourceAsStream("simple1.g4")));
		ANTLRv4Lexer lexer = new ANTLRv4Lexer(new ANTLRInputStream(new FileInputStream("/Users/peter/NetBeansProjects/workspace/Assembler/src/main/java/hk/quantr/assembler/antlr/AssemblerParser.g4")));
		CommonTokenStream tokenStream = new CommonTokenStream(lexer);
		ANTLRv4Parser parser = new ANTLRv4Parser(tokenStream);
		ANTLRv4Parser.GrammarSpecContext context = parser.grammarSpec();

		ParseTreeWalker walker = new ParseTreeWalker();
		MyANTLRv4ParserListener2 listener = new MyANTLRv4ParserListener2();
		walker.walk(listener, context);

	}

//	void loop(String s, AstNode n) {
//		System.out.println(s + n);
//		for (AstNode nn : n.getChildren()) {
//			loop(s + "    ", nn);
//		}
//	}
//
//	public static class JFrameWin extends JFrame {
//
//		public JFrameWin(BufferedImage bufferedImage) {
//			this.setSize(800, 800);
//			this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//
//			JLabel jLabel = new JLabel(new ImageIcon(bufferedImage));
//
//			JPanel jPanel = new JPanel();
//			jPanel.add(jLabel);
//			this.add(jPanel);
//		}
//
//	}
}

class MyANTLRv4ParserListener2 extends ANTLRv4ParserBaseListener {

	boolean bingo;

	@Override
	public void enterIdentifier(ANTLRv4Parser.IdentifierContext ctx) {
		if (ctx.getText().equals("tokenVocab")) {
			bingo = true;
		}
	}

	@Override
	public void enterOptionValue(ANTLRv4Parser.OptionValueContext ctx) {
		if (bingo) {
			System.out.println(ctx.getText());
			bingo = false;
		}
	}
}
