// License : Apache License Version 2.0  https://www.apache.org/licenses/LICENSE-2.0
package antlr;

import hk.quantr.javalib.CommonLib;
import org.junit.Test;
import org.openide.util.Exceptions;

/**
 *
 * @author Peter <mcheung63@hotmail.com>
 */
public class TestRunCommand {

	@Test
	public void run() {
		try {
			System.out.println(System.getProperty("java.home"));
//			String str = CommonLib.runCommand("C:\\Program Files (x86)\\Graphviz2.38\\bin\\dot.exe -Tpng C:\\Users\\Peter\\AppData\\Local\\Temp\\1.asm6434829820851445658.dot -o C:\\Users\\Peter\\desktop\\1.asm3568795673124410656.png");
//			String str = CommonLib.runCommand(new String[]{"\"" + System.getProperty("java.home") + "\\bin\\java.exe\"", "-version"});
//			System.out.println("\"" + System.getProperty("java.home") + "\\bin\\java.exe\"");
			String str = CommonLib.runCommand(System.getProperty("java.home") + "/bin/java -jar ../verilog-compiler/target/verilog-compiler-2.0.jar -m ../quantr-i/src/ex.v");
			System.out.println("str=" + str);
		} catch (Exception ex) {
			Exceptions.printStackTrace(ex);
		}
	}
}
