package antlr;

import hk.quantr.mxgraph.model.mxCell;
import hk.quantr.mxgraph.model.mxGeometry;
import hk.quantr.mxgraph.swing.mxGraphComponent;
import hk.quantr.mxgraph.swing.util.mxMorphing;
import hk.quantr.mxgraph.util.mxConstants;
import hk.quantr.mxgraph.util.mxEvent;
import hk.quantr.mxgraph.util.mxEventObject;
import hk.quantr.mxgraph.util.mxEventSource;
import hk.quantr.mxgraph.view.mxGraph;
import java.awt.BorderLayout;
import javax.swing.JFrame;
import org.junit.Test;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class TestJGraph {

	public static void main(String str[]){
		mxGraph graph = new mxGraph() {
			public boolean isPort(Object cell) {
				mxGeometry geo = getCellGeometry(cell);

				return (geo != null) ? geo.isRelative() : false;
			}

			public String getToolTipForCell(Object cell) {
				if (model.isEdge(cell)) {
					return convertValueToString(model.getTerminal(cell, true)) + " -> " + convertValueToString(model.getTerminal(cell, false));
				}

				return super.getToolTipForCell(cell);
			}

			public boolean isCellFoldable(Object cell, boolean collapse) {
				return false;
			}
		};
		//Object parent = graph.getDefaultParent();
		mxCell newNode = (mxCell) graph.insertVertex(null, null, "test 1", 40, 40, 150, 30);
		mxCell newNode2 = (mxCell) graph.insertVertex(null, null, "test 2", 140, 140, 150, 30);
		graph.insertEdge(null, null, "", newNode, newNode2, mxConstants.STYLE_STROKECOLOR + "=#ff0000;edgeStyle=elbowEdgeStyle;");

		graph.setCellsDisconnectable(false);
		mxGraphComponent graphComponent = new mxGraphComponent(graph);

		graph.getModel().beginUpdate();

		mxMorphing morph = new mxMorphing(graphComponent, 20, 1.2, 20);
		morph.addListener(mxEvent.DONE, new mxEventSource.mxIEventListener() {
			public void invoke(Object sender, mxEventObject evt) {
				graph.getModel().endUpdate();
			}
		});

		morph.startAnimation();

		JFrame frame = new JFrame();
		frame.setSize(800, 800);
		frame.setLayout(new BorderLayout());
		frame.add(graphComponent, BorderLayout.CENTER);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

}
