// License : Apache License Version 2.0  https://www.apache.org/licenses/LICENSE-2.0
package antlr;

import org.antlr.parser.ANTLRv4Lexer;
import org.antlr.parser.ANTLRv4Parser;
import org.antlr.runtime.RecognitionException;
import org.antlr.v4.Tool;
import org.antlr.v4.parse.ANTLRParser;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.tool.Grammar;
import org.antlr.v4.tool.ast.GrammarRootAST;
import org.junit.Test;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class TestInitRuleCombo {

	@Test
	public void test() throws RecognitionException {
		String content = "grammar VerilogMacro;\n"
				+ "\n"
				+ "compile\n"
				+ "	:	macro*\n"
				+ "	;\n"
				+ "\n"
				+ "macro\n"
				+ "	:	'peter'\n"
				+ "	;\n"
				+ "\n"
				+ "IDENTIFIER	:	[a-zA-Z0-9]+;\n"
				+ "\n"
				+ "White_space\n"
				+ "   : [ \\t\\n\\r] + -> channel (HIDDEN)\n"
				+ "   ;\n"
				+ "";

		Tool tool = new Tool();
		GrammarRootAST ast = tool.parseGrammarFromString(content);

//		ANTLRv4Lexer lexer = new ANTLRv4Lexer(new ANTLRInputStream(content));
//		CommonTokenStream tokenStream = new CommonTokenStream(lexer);
//		ANTLRv4Parser parser = new ANTLRv4Parser(tokenStream);
//		System.out.println(parser.getRuleNames().length);
		if (ast.grammarType == ANTLRParser.COMBINED || ast.grammarType == ANTLRParser.PARSER) {
			Grammar grammar = new Grammar(content);
			tool.process(grammar, false);

			for (String rule : grammar.getRuleNames()) {
				System.out.println(rule);
			}
		}
	}
}
