// License : Apache License Version 2.0  https://www.apache.org/licenses/LICENSE-2.0
package antlr;

import hk.quantr.netbeans.antlr.AntlrLib;
import hk.quantr.netbeans.antlr.ErrorListenerForDot;
import hk.quantr.netbeans.antlr.FileTypeG4VisualElement;
import hk.quantr.netbeans.antlr.ModuleLib;
import hk.quantr.netbeans.antlr.syntax.antlr4.Antlr4GeneralParserListener;
import hk.quantr.netbeans.antlr.syntax.antlr4.Ast;
import hk.quantr.netbeans.antlr.syntax.antlr4.AstNode;
import hk.quantr.javalib.CommonLib;
import hk.quantr.javalib.swing.advancedswing.jprogressbardialog.JProgressBarDialog;
import java.io.File;
import java.io.FileInputStream;
import javax.swing.ImageIcon;
import org.antlr.v4.Tool;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.LexerInterpreter;
import org.antlr.v4.runtime.ParserInterpreter;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.antlr.v4.tool.Grammar;
import org.antlr.v4.tool.LexerGrammar;
import org.antlr.v4.tool.Rule;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.openide.util.NbPreferences;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class TestDot {

	@Test
	public void testDot() throws Exception {
//		String lexBasicFile = "/Users/peter/NetBeansProjects/workspace/netbeans-antlr/src/main/java/hk/quantr/netbeans/antlr/syntax/antlrgenerate/LexBasic.g4";
//		String lexerGrammarFile = "/Users/peter/NetBeansProjects/workspace/netbeans-antlr/src/main/java/hk/quantr/netbeans/antlr/syntax/antlrgenerate/ANTLRv4Lexer.g4";
//		String parserGrammarFile = "/Users/peter/NetBeansProjects/workspace/netbeans-antlr/src/main/java/hk/quantr/netbeans/antlr/syntax/antlrgenerate/ANTLRv4Parser.g4";
//		File targetFile = new File("/Users/peter/NetBeansProjects/workspace/netbeans-antlr/src/test/resources/antlr/Assembler.g4");
//		String startRule = "grammarSpec";

		String lexerGrammarFile = "/Users/peter/NetBeansProjects/workspace/Assembler/src/main/java/hk/quantr/assembler/antlr/AssemblerLexer.g4";
		String parserGrammarFile = "/Users/peter/NetBeansProjects/workspace/Assembler/src/main/java/hk/quantr/assembler/antlr/AssemblerParser.g4";
		File targetFile = new File("/Users/peter/NetBeansProjects/workspace/Assembler/src/test/resources/hk/quantr/assembler/1.asm");
		String startRule = "assemble";
		
		String targetFileName = targetFile.getName();
		File pngTargetGraphFileName;

		Tool tool = new Tool();
		ErrorListenerForDot errorListenerForDot = new ErrorListenerForDot();
//		GrammarRootAST ast = tool.parseGrammarFromString(parserGrammarFile);

		String strLexer = IOUtils.toString(new FileInputStream(lexerGrammarFile), "utf-8");
//		String strLexBasic = IOUtils.toString(new FileInputStream(lexBasicFile), "utf-8");
		String strParser = IOUtils.toString(new FileInputStream(parserGrammarFile), "utf-8");
//		System.out.println(strParser);
		LexerGrammar lg = (LexerGrammar) Grammar.load(lexerGrammarFile);
//		LexerGrammar lg = new LexerGrammar(strLexer);
//		lg.importVocab(new LexerGrammar(strLexBasic));

//		Grammar grammar = Grammar.load(parserGrammarFile);
		Grammar grammar = new Grammar(strParser, lg);
		LexerInterpreter lexerInterpreter = lg.createLexerInterpreter(CharStreams.fromFileName(targetFile.getAbsolutePath()));
		lexerInterpreter.removeErrorListeners();
		lexerInterpreter.addErrorListener(errorListenerForDot);

		CommonTokenStream tokenStream = new CommonTokenStream(lexerInterpreter);
		System.out.println(grammar.getATN());
		ParserInterpreter parser = grammar.createParserInterpreter(tokenStream);
		parser.removeErrorListeners();
		parser.addErrorListener(errorListenerForDot);
		//parser.getInterpreter().setPredictionMode(PredictionMode.LL);
		Rule start = grammar.getRule(startRule);
		if (start == null) {
			ModuleLib.log("dot: start rule is null");
			return;
		}
		ParserRuleContext context = parser.parse(start.index);

		ParseTreeWalker walker = new ParseTreeWalker();
		Antlr4GeneralParserListener listener = new Antlr4GeneralParserListener(parser, lexerInterpreter);
		walker.walk(listener, context);

		Ast astNode = listener.ast;
		AstNode root = astNode.getRoot();
		root.setLabel(targetFile.getName());
		//AntlrLib.filterUnwantedSubNodes(root, new String[]{"ruleblock"});
		//AntlrLib.removeOneLeafNodes(root);
		AntlrLib.printAst("", root);

		String dot = AntlrLib.exportDot(root, errorListenerForDot.errorInfos, 10, "");

		File dotFile = new File("peter.dot");
		File dotPngFile = new File("peter.png");
		FileUtils.writeStringToFile(dotFile, dot, "UTF-8");
//				MutableGraph g = Parser.read(dot);
//				BufferedImage image = Graphviz.fromGraph(g).render(Format.PNG).toImage();
		JProgressBarDialog d = new JProgressBarDialog("Generating dot...", true);
		if (ModuleLib.isMac()) {
			System.out.println("/opt/local/bin/dot -Tpng " + dotFile.getAbsolutePath() + " -o " + dotPngFile.getAbsolutePath());
			CommonLib.runCommand("/opt/local/bin/dot -Tpng " + dotFile.getAbsolutePath() + " -o " + dotPngFile.getAbsolutePath());
		} else {
			String dotPath = NbPreferences.forModule(FileTypeG4VisualElement.class).get("dotPath", null);
			CommonLib.runCommand("\"" + dotPath + "\" -Tpng " + dotFile.getAbsolutePath() + " -o " + dotPngFile.getAbsolutePath());
		}
		AntlrLib.addText(dotPngFile);
		//Files.copy(dotPngFile.toPath(), new File("/Users/peter/Desktop/b.png").toPath(), REPLACE_EXISTING);
		ImageIcon icon = new ImageIcon(dotPngFile.getAbsolutePath());
		pngTargetGraphFileName = dotPngFile;
//				icon.getImage().flush();
//		preferWidth = graphvizLabel.getWidth() > icon.getIconWidth() ? icon.getIconWidth() : graphvizLabel.getWidth();
//		float ratio = ((float) preferWidth) / icon.getIconWidth();
//		if (ratio == 0) {
//			ratio = 1;
//		}
//		preferHeight = (int) (icon.getIconHeight() * ratio);
//		graphvizTargetGraphLabel.setIcon(resizeImage(icon, preferWidth, preferHeight));

//		dotFile.delete();
//		dotPngFile.deleteOnExit();
	}
}
