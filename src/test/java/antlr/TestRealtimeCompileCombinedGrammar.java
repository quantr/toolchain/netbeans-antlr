// License : Apache License Version 2.0  https://www.apache.org/licenses/LICENSE-2.0
package antlr;

import hk.quantr.netbeans.antlr.syntax.antlr4.Antlr4GeneralParserListener;
import static hk.quantr.netbeans.antlr.syntax.antlr4.errorhighlight.ErrorHighlightingTask.targetErrorInfos;
import hk.quantr.netbeans.antlr.syntax.antlr4.errorhighlight.ErrorInfo;
import hk.quantr.netbeans.antlr.syntax.antlr4.realtimecompile.RealTimeCompileErrorListener;
import java.io.File;
import java.io.FileInputStream;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.LexerInterpreter;
import org.antlr.v4.runtime.ParserInterpreter;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.antlr.v4.tool.Grammar;
import org.antlr.v4.tool.Rule;
import org.apache.commons.io.FileUtils;
import org.junit.Test;

/**
 *
 * @author Peter
 */
public class TestRealtimeCompileCombinedGrammar {

	@Test
	public void test() throws Exception {
		File grammarFile = new File("../antlr-calculator-library/src/main/java/hk/quantr/antlrcalculatorlibrary/antlr/Calculator.g4");
		File testFile = new File("../antlr-calculator-library/src/test/resources/test1.txt");

//		File grammarFile = new File("C:/users/peter/desktop/calculator.g4");
//		File testFile = new File("C:/users/peter/desktop/test1.txt");
		String content = FileUtils.readFileToString(grammarFile, "utf8");
		Grammar grammar = new Grammar(content);
		LexerInterpreter lexer = grammar.createLexerInterpreter(new ANTLRInputStream(new FileInputStream(testFile)));
		RealTimeCompileErrorListener targetErrorListener = new RealTimeCompileErrorListener(null, targetErrorInfos);
		lexer.removeErrorListeners();
		lexer.addErrorListener(targetErrorListener);

		lexer.addErrorListener(targetErrorListener);
		CommonTokenStream tokenStream = new CommonTokenStream(lexer);
		ParserInterpreter parser = grammar.createGrammarParserInterpreter(tokenStream);
		parser.removeErrorListeners();
		parser.addErrorListener(targetErrorListener);

		Rule start = grammar.getRule("input");
		ParserRuleContext parserRuleContext = parser.parse(start.index);
		ParseTreeWalker walker = new ParseTreeWalker();
		Antlr4GeneralParserListener listener = new Antlr4GeneralParserListener(parser, lexer);
		walker.walk(listener, parserRuleContext);
		System.out.println("targetErrorInfos.size=" + targetErrorInfos.size());
		for (ErrorInfo errorInfo : targetErrorInfos) {
			System.out.println(errorInfo);
		}

		for (Token t : tokenStream.getTokens()) {
			System.out.println("token=" + t.getText());
		}
	}
}
