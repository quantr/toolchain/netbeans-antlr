// License : Apache License Version 2.0  https://www.apache.org/licenses/LICENSE-2.0
package antlr;

import com.khubla.antlr4formatter.Antlr4Formatter;
import java.io.FileInputStream;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class TestFormatter {

	@Test
	public void test() throws Exception {
		String s = IOUtils.toString(new FileInputStream("C:\\workspace\\Assembler\\src\\main\\java\\hk\\quantr\\assembler\\antlr\\RISCVAssemblerParser.g4"), "utf8");
		System.out.println(Antlr4Formatter.format(s));
	}
}
