// License : Apache License Version 2.0  https://www.apache.org/licenses/LICENSE-2.0
package antlr;

import hk.quantr.netbeans.antlr.syntax.antlr4.MyANTLRv4ParserListener;
import hk.quantr.netbeans.antlr.syntax.antlr4.errorhighlight.ErrorHighlightingTask;
import hk.quantr.netbeans.antlr.syntax.antlr4.errorhighlight.ErrorInfo;
import java.io.File;
import java.util.BitSet;
import org.antlr.parser.ANTLRv4Lexer;
import org.antlr.parser.ANTLRv4Parser;
import org.antlr.v4.runtime.ANTLRErrorListener;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.atn.ATNConfigSet;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.apache.commons.io.FileUtils;
import org.junit.Test;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class TestAntlr4ParserErrorHighlight {

	@Test
	public void test1() throws Exception {
		String file = FileUtils.readFileToString(new File("/Users/peter/workspace/Assembler/src/main/java/hk/quantr/assembler/antlr/Assembler.g4"), "utf8");
		ANTLRv4Lexer lexer = new ANTLRv4Lexer(new ANTLRInputStream(file));
		CommonTokenStream tokenStream = new CommonTokenStream(lexer);
		ANTLRv4Parser parser = new ANTLRv4Parser(tokenStream);
		ErrorHighlightingTask.errorInfos.clear();
		parser.addErrorListener(new ANTLRErrorListener() {
			@Override
			public void syntaxError(Recognizer<?, ?> rcgnzr, Object offendingSymbol, int lineNumber, int charOffsetFromLine, String message, RecognitionException re) {
				System.out.println("Antlr4Parser::syntaxError");
				Token offendingToken = (Token) offendingSymbol;
				int start = offendingToken.getStartIndex();
				int stop = offendingToken.getStopIndex();
				ErrorHighlightingTask.errorInfos.add(new ErrorInfo(start, stop, message));
			}

			@Override
			public void reportAmbiguity(org.antlr.v4.runtime.Parser parser, DFA dfa, int i, int i1, boolean bln, BitSet bitset, ATNConfigSet atncs) {
				System.out.println("Antlr4Parser::reportAmbiguity");
			}

			@Override
			public void reportAttemptingFullContext(org.antlr.v4.runtime.Parser parser, DFA dfa, int i, int i1, BitSet bitset, ATNConfigSet atncs) {
				System.out.println("Antlr4Parser::reportAttemptingFullContext");
			}

			@Override
			public void reportContextSensitivity(org.antlr.v4.runtime.Parser parser, DFA dfa, int i, int i1, int i2, ATNConfigSet atncs) {
				System.out.println("Antlr4Parser::reportContextSensitivity");
			}
		});
		ANTLRv4Parser.GrammarSpecContext context = parser.grammarSpec();
		ParseTreeWalker walker = new ParseTreeWalker();
		MyANTLRv4ParserListener listener = new MyANTLRv4ParserListener(null, parser);
		walker.walk(listener, context);
	}
}
