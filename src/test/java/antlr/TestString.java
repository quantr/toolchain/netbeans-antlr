// License : Apache License Version 2.0  https://www.apache.org/licenses/LICENSE-2.0
package antlr;

import hk.quantr.netbeans.antlr.TreeTopComponent;
import hk.quantr.netbeans.antlr.syntax.antlr4.Ast;
import hk.quantr.netbeans.antlr.syntax.antlr4.AstNode;
import static hk.quantr.netbeans.antlr.syntax.antlr4.errorhighlight.ErrorHighlightingTask.targetErrorInfos;
import hk.quantr.netbeans.antlr.syntax.antlr4.errorhighlight.ErrorInfo;
import hk.quantr.netbeans.antlr.syntax.antlr4.realtimecompile.RealTimeCompileErrorListener;
import java.io.FileInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.Predicate;
import org.antlr.parser.ANTLRv4ParserBaseListener;
import org.antlr.v4.Tool;
import org.antlr.v4.parse.ANTLRParser;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.LexerInterpreter;
import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.ParserInterpreter;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.RuntimeMetaData;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.atn.PredictionMode;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.antlr.v4.tool.Grammar;
import org.antlr.v4.tool.Rule;
import org.antlr.v4.tool.ast.GrammarRootAST;
import org.junit.Test;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class TestString {

	@Test
	public void test() throws Exception {
		System.out.println("realTimeCompile, antlr version=" + RuntimeMetaData.VERSION);
		String content = new String(Files.readAllBytes(Paths.get("/Users/peter/workspace/Assembler/src/main/java/hk/quantr/assembler/antlr/Assembler.g4")));
		Tool tool = new Tool();
		GrammarRootAST ast = tool.parseGrammarFromString(content);
		targetErrorInfos.clear();
		if (ast.grammarType == ANTLRParser.COMBINED) {
			ArrayList<ErrorInfo> targetErrorInfos = new ArrayList<>();
			RealTimeCompileErrorListener targetErrorListener = new RealTimeCompileErrorListener(null, targetErrorInfos);
			Grammar grammar = new Grammar(content);
			LexerInterpreter lexer = grammar.createLexerInterpreter(new ANTLRInputStream(new FileInputStream("/Users/peter/workspace/Assembler/src/test/resources/hk/quantr/assembler/1.asm")));
			lexer.removeErrorListeners();
			lexer.addErrorListener(targetErrorListener);

			CommonTokenStream tokenStream = new CommonTokenStream(lexer);
			ParserInterpreter parser = grammar.createParserInterpreter(tokenStream);
			parser.getInterpreter().setPredictionMode(PredictionMode.LL);
			parser.removeErrorListeners();

			Rule start = grammar.getRule("assemble");
			ParserRuleContext parserRuleContext = parser.parse(start.index);
			ParseTreeWalker walker = new ParseTreeWalker();
			Antlr4GeneralParserListener2 listener = new Antlr4GeneralParserListener2(parser, lexer);
			walker.walk(listener, parserRuleContext);

			for (Token t : tokenStream.getTokens()) {
				System.out.println("token=" + t.getText());
			}

//			Grammar grammar = new Grammar(content);
//			LexerInterpreter lexer = grammar.createLexerInterpreter(new ANTLRInputStream(new FileInputStream("/Users/peter/workspace/Assembler/src/test/resources/hk/quantr/assembler/1.asm")));
//			lexer.removeErrorListeners();
//
//			CommonTokenStream tokenStream = new CommonTokenStream(lexer);
//			ParserInterpreter parser = grammar.createParserInterpreter(tokenStream);
//			//parser.getInterpreter().setPredictionMode(PredictionMode.LL);
//			Rule start = grammar.getRule("assemble");
//			if (start == null) {
//				ModuleLib.log("dot: start rule is null");
//				return;
//			}
//			ParserRuleContext context = parser.parse(start.index);
//
//			ParseTreeWalker walker = new ParseTreeWalker();
//			Antlr4GeneralParserListener listener = new Antlr4GeneralParserListener(parser, lexer);
//			walker.walk(listener, context);
//
//			Ast astNode = listener.ast;
//			AstNode root = astNode.getRoot();
//			root.setLabel("fuck");
//			//AntlrLib.filterUnwantedSubNodes(root, new String[]{"ruleblock"});
//			//AntlrLib.removeOneLeafNodes(root);
//			AntlrLib.printAst("", root);
//
//			String dot = AntlrLib.exportDot(root);
//			System.out.println(dot);
		}
	}
}

class Antlr4GeneralParserListener2 extends ANTLRv4ParserBaseListener {

	LexerInterpreter lexer;
	Parser parser;
	public Ast ast = null;
	private final Map<String, Integer> rmap = new HashMap<>();
	Predicate<String> filter = x -> !x.isEmpty();
	AstNode parentNode = null;
	String content;

	public Antlr4GeneralParserListener2(Parser parser, LexerInterpreter lexer) {
		this.lexer = lexer;
		this.parser = parser;
		ast = new Ast("root", "root");
		content = lexer.getInputStream().toString();
		parentNode = ast.getRoot();
		rmap.putAll(parser.getRuleIndexMap());
//		if (TreeTopComponent.treeTopComponent != null) {
//			Antlr4TokenTableModel parserTableModel = TreeTopComponent.treeTopComponent.parserTableModel;
//			if (parserTableModel != null) {
//				parserTableModel.tokens.clear();
//			}
//		}
	}

	@Override
	public void enterEveryRule(ParserRuleContext ctx) {
		String ruleName = getRuleByKey(ctx.getRuleIndex());
		String thisContent = content.substring(ctx.getStart().getStartIndex(), ctx.getStop().getStopIndex() + 1);
		System.out.println(ruleName + " = " + ctx.getStart().getStartIndex() + " , " + ctx.getStop().getStopIndex() + " = " + thisContent + " = " + ctx.getText());
		String text = ctx.getText();
		Token s = ctx.getStart();
		Token e = ctx.getStop();
		AstNode n = ast.newNode(parentNode, ruleName, text, s != null ? s.getStartIndex() : 0, e != null ? e.getStopIndex() : 0);
		parentNode.addChild(n);
		parentNode = n;
	}

	@Override
	public void exitEveryRule(ParserRuleContext ctx) {
		//String ruleName = getRuleByKey(ctx.getRuleIndex());(ParserRuleContext ctx) {
		parentNode = parentNode.getParent();
	}

	public String getRuleByKey(int key) {
		return rmap.entrySet().stream().filter(e -> Objects.equals(e.getValue(), key)).map(Map.Entry::getKey).findFirst().orElse(null);
	}

	@Override
	public void visitTerminal(TerminalNode tn) {
		String temp = parser.getVocabulary().getSymbolicName(tn.getSymbol().getType());
		System.out.println("visitTerminal = " + temp + " - " + tn.getText());
		if (TreeTopComponent.treeTopComponent == null) {
			return;
		}
		AstNode n = ast.newNode(parentNode, temp, tn.getText(), tn.getSymbol() != null ? tn.getSymbol().getStartIndex() : 0, tn.getSymbol() != null ? tn.getSymbol().getStopIndex() : 0);
		parentNode.addChild(n);

//		Antlr4TokenTableModel tokenTableModel = TreeTopComponent.treeTopComponent.parserTableModel;
//		if (tokenTableModel != null) {
//			ArrayList<TokenInfo> list = tokenTableModel.tokens.get(tn.getSymbol().getLine());
//			if (list == null) {
//				list = new ArrayList<>();
//			}
////			if (temp == null) {
////				temp = tn.getText();
////			}
//			list.add(new TokenInfo(tn.getSymbol().getLine(), temp, tn.getSymbol().getStartIndex(), tn.getSymbol().getStopIndex()));
//
//			tokenTableModel.tokens.put(tn.getSymbol().getLine(), list);
////			ModuleLib.log(tn.getSymbol().getLine() + " = " + parser.getTokenNames()[tn.getSymbol().getName()] + " > " + tn.getText());
//			//ModuleLib.log(tn.getSymbol().getLine() + " = " + parser.getVocabulary().getSymbolicName(tn.getSymbol().getName()) + " > " + tn.getText() + " , " + tn.getSymbol().getStartIndex() + " > " + tn.getSymbol().getStopIndex());
//		}
	}

	@Override
	public void visitErrorNode(ErrorNode en) {
	}

}
