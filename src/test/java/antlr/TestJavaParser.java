// License : Apache License Version 2.0  https://www.apache.org/licenses/LICENSE-2.0
package antlr;

import hk.quantr.javalib.CommonLib;
import hk.quantr.javalib.swing.advancedswing.jprogressbardialog.JProgressBarDialog;
import hk.quantr.netbeans.antlr.AntlrLib;
import hk.quantr.netbeans.antlr.ErrorListenerForDot;
import hk.quantr.netbeans.antlr.FileTypeG4VisualElement;
import hk.quantr.netbeans.antlr.ModuleLib;
import hk.quantr.netbeans.antlr.syntax.antlr4.Antlr4GeneralParserListener;
import hk.quantr.netbeans.antlr.syntax.antlr4.Ast;
import hk.quantr.netbeans.antlr.syntax.antlr4.AstNode;
import java.io.File;
import java.io.FileInputStream;
import javax.swing.ImageIcon;
import org.antlr.v4.Tool;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.LexerInterpreter;
import org.antlr.v4.runtime.ParserInterpreter;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.antlr.v4.tool.Grammar;
import org.antlr.v4.tool.LexerGrammar;
import org.antlr.v4.tool.Rule;
import org.antlr.v4.tool.ast.GrammarRootAST;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.openide.util.NbPreferences;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class TestJavaParser {

	@Test
	public void test() throws Exception {
		File targetFile = new File("../quantr-i/src/ex.v");
		String targetFileName = targetFile.getName();

		Tool tool = new Tool();
		ErrorListenerForDot errorListenerForDot = new ErrorListenerForDot();

		String strParser = IOUtils.toString(new FileInputStream("../verilog-compiler/src/main/java/hk/quantr/verilogcompiler/antlr/VerilogPreParser.g4"), "utf-8");
		GrammarRootAST ast = tool.parseGrammarFromString(strParser);
		File lexerGrammarFile = new File("../verilog-compiler/src/main/java/hk/quantr/verilogcompiler/antlr/VerilogLexer.g4");
		if (!lexerGrammarFile.isFile() || !lexerGrammarFile.exists()) {
			ModuleLib.log("refreshTargetJgraphButtonActionPerformed lexerGrammarFile is null");
		} else {
			String strLexer = IOUtils.toString(new FileInputStream(lexerGrammarFile), "utf-8");
			LexerGrammar lg = new LexerGrammar(strLexer);
			Grammar grammar = new Grammar(strParser, lg);

			LexerInterpreter lexerInterpreter = lg.createLexerInterpreter(new ANTLRInputStream(new FileInputStream(targetFile)));
			lexerInterpreter.removeErrorListeners();
			lexerInterpreter.addErrorListener(errorListenerForDot);

			CommonTokenStream tokenStream = new CommonTokenStream(lexerInterpreter, 3);
			System.out.println("ATN: "+grammar.getATN());
			ParserInterpreter parser = grammar.createParserInterpreter(tokenStream);
			parser.removeErrorListeners();
			parser.addErrorListener(errorListenerForDot);
			//parser.getInterpreter().setPredictionMode(PredictionMode.LL);
			String startRule = "source_text";
			Rule start = grammar.getRule(startRule);
			if (start == null) {
				ModuleLib.log("dot: start rule is null");
				return;
			}
			ParserRuleContext context = parser.parse(start.index);

			ParseTreeWalker walker = new ParseTreeWalker();
			Antlr4GeneralParserListener listener = new Antlr4GeneralParserListener(parser, lexerInterpreter);
			walker.walk(listener, context);

			Ast astNode = listener.ast;
			AstNode root = astNode.getRoot();
			root.setLabel(targetFile.getName());
			//AntlrLib.filterUnwantedSubNodes(root, new String[]{"ruleblock"});
			//AntlrLib.removeOneLeafNodes(root);
			AntlrLib.printAst("", root);

			/*
			this.dot = AntlrLib.exportDot(root, errorListenerForDot.errorInfos);

			File dotFile = File.createTempFile(targetFileName, ".dot");
			File dotPngFile = File.createTempFile(targetFileName, ".png");
			FileUtils.writeStringToFile(dotFile, this.dot, "UTF-8");
//				MutableGraph g = Parser.read(dot);
//				BufferedImage image = Graphviz.fromGraph(g).render(Format.PNG).toImage();
			JProgressBarDialog d = new JProgressBarDialog("Generating dot...", true);
			d.progressBar.setIndeterminate(true);
			d.progressBar.setStringPainted(true);
			d.progressBar.setString("running dot command : " + "dot -Tpng " + dotFile.getAbsolutePath() + " -o " + dotPngFile.getAbsolutePath());
//					if (ModuleLib.isMac()) {
//						CommonLib.runCommand("/opt/local/bin/dot -Tpng " + dotFile.getAbsolutePath() + " -o " + dotPngFile.getAbsolutePath());
//					} else {
			String dotPath = NbPreferences.forModule(FileTypeG4VisualElement.class).get("dotPath", null);
			if (dotPath.contains(" ")) {
				dotPath = "\"" + dotPath + "\"";
			}
			graphvizTargetGraphLabel.setText(CommonLib.runCommand(dotPath + " -Tpng " + dotFile.getAbsolutePath() + " -o " + dotPngFile.getAbsolutePath()));
//					}
			//Files.copy(dotPngFile.toPath(), new File("/Users/peter/Desktop/b.png").toPath(), REPLACE_EXISTING);
			if (dotPngFile.exists()) {
				AntlrLib.addText(dotPngFile);
				ImageIcon icon = new ImageIcon(dotPngFile.getAbsolutePath());
				pngTargetGraphFileName = dotPngFile;
//				icon.getImage().flush();
				preferWidth = graphvizLabel.getWidth() > icon.getIconWidth() ? icon.getIconWidth() : graphvizLabel.getWidth();
				float ratio = ((float) preferWidth) / icon.getIconWidth();
				if (ratio == 0) {
					ratio = 1;
				}
				preferHeight = (int) (icon.getIconHeight() * ratio);
				graphvizTargetGraphLabel.setIcon(resizeImage(icon, preferWidth, preferHeight));
				graphvizTargetGraphLabel.setText(null);

				dotPngFile.deleteOnExit();
			}
			dotFile.delete();
			*/
		}
	}
}
