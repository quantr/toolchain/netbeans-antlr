// License : Apache License Version 2.0  https://www.apache.org/licenses/LICENSE-2.0
package antlr;

import hk.quantr.netbeans.antlr.Antlr4TokenTableModel;
import hk.quantr.netbeans.antlr.ModuleLib;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.LexerInterpreter;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.tool.Grammar;
import org.antlr.v4.tool.LexerGrammar;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

/**
 *
 * @author peter
 */
public class TestTokenTable {

	@Test
	public void test() throws Exception {
		Antlr4TokenTableModel tokenTableModel = new Antlr4TokenTableModel();
		File lexerGrammarFile = new File("/Users/peter/NetBeansProjects/workspace/Assembler/src/main/java/hk/quantr/assembler/antlr/AssemblerLexer.g4");
		File grammarFile = new File("/Users/peter/NetBeansProjects/workspace/Assembler/src/main/java/hk/quantr/assembler/antlr/AssemblerPArser.g4");
		if (lexerGrammarFile == null || !lexerGrammarFile.isFile() || !lexerGrammarFile.exists()) {
			System.out.println("realTimeCompile lexerGrammarFile is null");
		} else {
			String strLexer = IOUtils.toString(new FileInputStream(lexerGrammarFile), "utf-8");
			String strParser = IOUtils.toString(new FileInputStream(grammarFile), "utf-8");
			LexerGrammar lg = new LexerGrammar(strLexer);
			Grammar grammar = new Grammar(strParser, lg);

			String targetFileContent = "(bits 16){\n"
					+ "	adc al, 12\n"
					+ "}";

			ModuleLib.log("refreshTokenTable =" + targetFileContent.substring(0, 15));
			LexerInterpreter lexerInterpreter = lg.createLexerInterpreter(new ANTLRInputStream(targetFileContent));

			tokenTableModel.vocabulary = lexerInterpreter.getVocabulary();
			int lastLine = -1;
			ArrayList<Token> tokens = new ArrayList<Token>();
			for (Token token : lexerInterpreter.getAllTokens()) {
				int lineNo = token.getLine();
				if (lineNo != lastLine) {
					tokens = new ArrayList<Token>();
					lastLine = lineNo;
				}
				String tokenName = lexerInterpreter.getVocabulary().getSymbolicName(token.getType());
				System.out.println("refreshTokenTable ---------------- " + lineNo + " = " + tokenName);
				tokens.add(token);

				tokenTableModel.tokens.put(lineNo, tokens);
			}
		}
	}
}
