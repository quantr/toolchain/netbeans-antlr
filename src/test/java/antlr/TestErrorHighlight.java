// License : Apache License Version 2.0  https://www.apache.org/licenses/LICENSE-2.0
package antlr;

import hk.quantr.netbeans.antlr.syntax.antlr4.Antlr4GeneralParserListener;
import java.io.FileInputStream;
import org.antlr.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.ANTLRFileStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.LexerInterpreter;
import org.antlr.v4.runtime.ParserInterpreter;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.atn.PredictionMode;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.antlr.v4.tool.Grammar;
import org.antlr.v4.tool.LexerGrammar;
import org.antlr.v4.tool.Rule;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class TestErrorHighlight {

	@Test
	public void test() throws Exception {
		String strLexer = IOUtils.toString(new FileInputStream("/Users/peter/workspace/TestAntlr/src/main/java/com/peter/testantlr/antlr/JavaLexer.g4"));
		String strParser = IOUtils.toString(new FileInputStream("/Users/peter/workspace/TestAntlr/src/main/java/com/peter/testantlr/antlr/JavaParser.g4"));
		LexerGrammar lg = new LexerGrammar(strLexer);
//		LexerGrammar lg = (LexerGrammar) Grammar.load("/Users/peter/workspace/TestAntlr/src/main/java/com/peter/testantlr/antlr/JavaLexer.g4");
		Grammar grammar = new Grammar(strParser);
//		Grammar grammar = tool.createGrammar(ast);
		ANTLRFileStream input = new ANTLRFileStream("/Users/peter/workspace/pfsbuilder/src/main/java/com/pfsbuilder/MainFrame2.java");
//		String input = IOUtils.toString(new FileInputStream("/Users/peter/workspace/pfsbuilder/src/main/java/com/pfsbuilder/MainFrame2.java"));
		LexerInterpreter lexEngine = lg.createLexerInterpreter(input);

		CommonTokenStream tokenStream = new CommonTokenStream(lexEngine);
		//tokenStream.fill();
		System.out.println("tokenStream=" + tokenStream);
		System.out.println("grammar=" + grammar);
		System.out.println("tokenStream.getTokens=" + tokenStream.getTokens().size());
//		for (Token token : tokenStream.getTokens()) {
//			System.out.println(token);
//		}
//		grammar.importVocab(lg);
		System.out.println(grammar.getATN());
		ParserInterpreter parser = grammar.createParserInterpreter(tokenStream);
		parser.getInterpreter().setPredictionMode(PredictionMode.LL);

		String startRule = "compilationUnit";
		Rule start = grammar.getRule(startRule);
		ParserRuleContext parserRuleContext = parser.parse(start.index);
		ParseTreeWalker walker = new ParseTreeWalker();
		//Antlr4GeneralParserListener listener = new Antlr4GeneralParserListener(parser, lexerInterpreter);
		//walker.walk(listener, parserRuleContext);
	}
	
	

}
